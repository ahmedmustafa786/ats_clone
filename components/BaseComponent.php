<?php

namespace app\components;

use Yii;
use app\models\Settings;
use app\models\User;
use app\models\Countries;
use app\models\UserrolesModulelists;
use yii\base\Component;
use yii\base\InvalidConfigException;

class BaseComponent extends Component
{
	public $settings = array();
    public $userroles= array();
    public $countries= array();
    public $notifications = array();
    public $companyId = 0;

    public function init(){
        parent::init();

        $settingsRecords = Settings::find()->all();

        foreach($settingsRecords as $s){
        	$this->settings[$s->name] = $s->value;
        }
        //checking permissions

        $currentUrl     = explode('/', Yii::$app->request->url);
        $controller     = Yii::$app->controller->id;
        $action         = Yii::$app->controller->action->id;
   
        $this->countries= Countries::find()->where(['status' => 1])->all();

        // get notifications
        /*if(!Yii::$app->user->isGuest){ 
            $loggedInUserId = Yii::$app->user->getId();
            $user           = User::find()->where(['users.id' => $loggedInUserId])->joinWith('usersUserroles')->one();
            $notificationRoles=array();
            foreach($user->usersUserroles as $roles){
                $notificationRoles[]= $roles->userrole_id;
            }

            if(in_array('4', $notificationRoles)){
                $notifResult = \app\models\Notifications::find()
                                                        ->where(['user_id' => $loggedInUserId, 'is_read' => '0'])
                                                        ->limit(5)
                                                        ->all();
            }else{
                $notifResult = \app\models\Notifications::find()
                                                        ->where(['is_read' => '0', 'user_id' => null])
                                                        ->limit(5)
                                                        ->all();
            }
            // \app\components\Utility::debug($notifResult);exit;
            if($notifResult){
                foreach($notifResult as $noti){
                    $this->notifications[$noti->id] = $noti->notificationtext;
                }
            }
        }*/
       /* if($action == ''){

        }else if($action != 'notfound'){ 

            if(!Yii::$app->user->isGuest){ 
                $loggedInUserId = Yii::$app->user->getId();
                // \app\components\Utility::debug($loggedInUserId);
                // exit;
                        
                $user           = User::find()->where(['users.id' => $loggedInUserId])->joinWith('usersUserroles')->one();

                
                foreach($user->usersUserroles as $roles){ 
                    $this->userroles[]= $roles->userrole_id;
                }

                if(in_array(6, $this->userroles)){
                    $companyIdData = \app\models\UsersCompanies::find()->where(['user_id' => $loggedInUserId])->one();
                    $this->companyId=$companyIdData->company_id;
                }
                
                if($action != 'notfound' 
                && !in_array(3, $this->userroles)){
                    
                    $policyRoles    = UserrolesModulelists::find()
                                                        ->where(['in', 'userrole_id', implode(',', $this->userroles)])
                                                        ->joinWith('modulelist')
                                                        ->all();

                    foreach($policyRoles as $pr){
                        $tempController[]     = strtolower($pr->modulelist->module->title);
                        $tempAction[]         = strtolower($pr->modulelist->title);
                    }
                    /*echo "<pre>";
                    print_r($tempAction);
                    print_r($tempController);
                    print_r($action);
                    print_r($controller);
                    echo "I am here"; exit;*/
                    /*if(!in_array($controller, $tempController)
                    || !in_array($action, $tempAction)){
                        return Yii::$app->getResponse()->redirect(['/users/notfound']);
                    }
                }
            }else{

                return Yii::$app->getResponse()->redirect(['/site/index']);
            }
        }*/
    }

    public function render($view, $params = array()) 
    {
        $params = array_merge($this->settings, $params);
        return parent::render($view, $params);
    }

    public function getRandomPassword(){
        $length = 10;
        $chars = array_merge(range(0,9), range('a','z'), range('A','Z'));
        shuffle($chars);
        $password = implode(array_slice($chars, 0, $length));
        return $password;
    }

}