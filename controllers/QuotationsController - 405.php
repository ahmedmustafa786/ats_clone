<?php

namespace app\controllers;

use Yii;
use app\models\Quotations;
use app\models\QuotationsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use mPDF;
use app\models\Catchbasin;
use yii\helpers\ArrayHelper;						  

/**
 * QuotationsController implements the CRUD actions for Quotations model.
 */
class QuotationsController extends Controller
{
    public $existingDrains  = array();
    public $checkLength     = 0;
    public $allNodes        = array();
    public $preMaxLength    = 0;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Quotations models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QuotationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Quotations model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $userId = Yii::$app->user->getId();
        $query  = \app\models\Notifications::find()->where(['is_read' => '0', 'user_id' => $userId]);
        $query->andWhere("notificationtext like '%recordId=".$id."%'");
        $notificationData = $query->one();
        if($notificationData){
            $notificationData->is_read = 1;
            $notificationData->update_date = date('Y-m-d H:i:s');
            $notificationData->save();
        }
        

        $quoteRow = Quotations::find()->where(['id' => $id])->one();

        if(empty($quoteRow)){
            Yii::$app->getSession()->setFlash('error', 'Configuration has been removed !');
            return $this->redirect(['/jobs/list']);
        }
        return $this->render('view', [
             'quoteRow' => $quoteRow,
        ]);

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Quotations model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {   

        $model = new Quotations();
        $modelFeature = new \app\models\FeaturesAll();
		 // Get Catch Basin Model by AM (Start)
        $modelCatchbasin = new \app\models\Catchbasin();
        // Get Catch Basin Model by AM (End)																																																																																					
        $modelFeatures = yii\helpers\ArrayHelper::map(\app\models\Featuretypes::find()->all(),'id','title');
        $modelGrateMaterial = yii\helpers\ArrayHelper::map(\app\models\FeaturesAll::find()->groupBy('grate_material')->all(),'suffix','grate_material');
        $modelGrateLoadRating = yii\helpers\ArrayHelper::map(\app\models\FeaturesAll::find()->groupBy('grate_load_rating')->all(),'id','grate_load_rating');
		
		// Get Catch Basin List of size by AM (Start)
        $lists = Catchbasin::find()->groupBy('catch_basin_size')->all();
        foreach($lists as $list){
        $a = $list->catch_basin_size = $list->catch_basin_size.'"'." Width";
        $b = $list->id = $list->catch_basin_size;
        }
        $modelCatchbasinsize = ArrayHelper::map($lists, 'catch_basin_size', 'catch_basin_size');
        // $modelCatchbasinsize = yii\helpers\ArrayHelper::map(\app\models\Catchbasin::find()->groupBy('catch_basin_size')->all(),'catch_basin_size', $list);
        // \app\components\Utility::debug($modelCatchbasinsize);exit;
        // Get Catch Basin List of size by AM (End)						 											   
        if (Yii::$app->request->post()) {
            $jobID = Yii::$app->request->get('jobid');
            $this::Calculation($jobID);
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelFeatures'=>$modelFeatures,
                'modelFeature'=> $modelFeature,
                'modelGrateMaterial'=>$modelGrateMaterial,
                'modelGrateLoadRating'=>$modelGrateLoadRating,
				'modelCatchbasin'=>$modelCatchbasin,
                'modelCatchbasinsize'=>$modelCatchbasinsize,										  									
            ]);
        }
    }
		// Get Catch Basin Model by AM (Start)
	    public function actionGetCatchBasinSize() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) { 
                $trench_frame_id = $parents[0];
                $out = self::getCatchBasinSizeList($trench_frame_id); 
                echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }
		// Get Catch Basin Model by AM (End)
		// Get Catch Basin Model List by AM (Start)
    public static function getCatchBasinSizeList($trench_frame_id){
        $data= \app\models\Catchbasin::find()->groupBy('catch_basin_size')
           ->where(['series_id'=>$trench_frame_id])
           ->select(['catch_basin_size as id',"(concat_ws('\" ',catch_basin_size,'Width')) AS name"])->asArray()->all();
        return $data;
    }										   					 
	// Get Catch Basin Model List by AM (End)
    //Simple Calculation 
    /*public function SimpleCalculation($jobID){

        $length = 53;
        $mod = $length%4;
        $divider = floor($length/4);
        if($mod == 0){

        }else{

            $innerDivider   = floor($divider/5); //full series
            $innderMod      =   $divider%5; //Remainig parts of series
            $remainingPart  = '';
            $drainsQuery    = \app\models\Drains::find()
            ->leftJoin('drainproducts as ds','drains.id=ds.drain_id')
            ->where(['drains.series_id' => 1])
            ->andWhere("ds.type='n' and drains.config = 'Sloped' and ds.product_code not like '%_N'")
            ->limit($divider)
            ->all();
            echo "<pre>";
            print_r($drainsQuery);
            exit;
            for ($seriesIndex=0;$seriesIndex<$innerDivider;$seriesIndex++) 
            {
                # code...

            }
        }
    }*/

    public function Calculation($jobID){
        if(Yii::$app->request->post()){
            $inputs = $nodes = $lines = array();
            foreach(Yii::$app->request->post() as $key => $val){
                if($val != ''){
                    if(strpos($key, 'nodes') > -1){
                        $key = str_replace('nodes_', '', $key);
                        preg_match('/images\/(.*).svg/', $val, $match);
                        $val = $match[1];
                        $nodes[$key] = $val;
                    }else if(strpos($key, 'lines') > -1){
                        $key = str_replace('lines_', '', $key);
                        $lines[$key] = $val;
                    }else if($key != '_csrf'){
                        $inputs[$key] = $val;
                    }
                    $inputs['nodes'] = $nodes;
                    $inputs['lines'] = $lines;
                    // echo $key.'---'.$val.'<br>';
                }
            }
            // exit;
        }

        if(count($inputs['lines']) == 0){
            Yii::$app->getSession()->setFlash('error', 'Please select Length from Grid!');
             return $this->redirect(['create','jobid'=>$jobID]);
        }
        $horPosition = $varPosition = '';
        $nodePosition = array_keys($inputs['nodes'])[0];
        foreach($lines as $key => $val){
            if(strpos($key, 'hor_') > -1){
                $horPosition = str_replace('hor_', '', $key);
            }
            if(strpos($key, 'var_') > -1){
                $varPosition = str_replace('var_', '', $key);
            }
        }
        
        if(!empty($horPosition)){
            list($xH, $yH) = explode('_', $horPosition);
        }
        if(!empty($varPosition)){
            list($xV, $yV) = explode('_', $varPosition);
        }
        if(!empty($nodePosition)){
            list($xO, $yO) = explode('_', $nodePosition);
        }

        $outletPosition = array();
        if($nodePosition == $horPosition && $nodePosition == $varPosition){
            $outletPosition = ['top', 'left'];
        }else if($horPosition == $nodePosition){
            $outletPosition = ['bottom', 'left'];
        }else if($varPosition == $nodePosition){
            $outletPosition = ['top', 'right'];
        }else if(@$xH == $xO && @$yH < $yO){
            $outletPosition = ['bottom', 'right'];
        }
        if(!isset($outletPosition[0])){
            // echo $xO.'...'.$xV;
            if ($xO < $xV){
                $outletPosition = ['top', 'right'];
            }else{
                $outletPosition = ['bottom', 'right'];
            }
        }
        $inputs['outletPosition'] = $outletPosition;
        // Get Catch Basin value by AM (Start)
        $outletType = '';
        
                if(!empty($inputs['Catchbasin']['catch_basin_size']))
                   {
                    $catch_basin_size = $inputs['Catchbasin']['catch_basin_size'];
                   }
                else
                   {
                    $catch_basin_size ='';
                   }
                if(!empty($inputs['Catchbasin']['trash_basket']))
                   {
                    $trash_basket = $inputs['Catchbasin']['trash_basket'];
                   }
                else
                   {
                    $trash_basket ='';
                   }
                // \app\components\Utility::debug($trash_basket);exit;
        // Get Catch Basin value by AM (End)										  
        // \app\components\Utility::debug($outletPosition);
        // \app\components\Utility::debug($inputs);
        // exit;
        $equal = 0;
        $x = 0;
        foreach($inputs['lines'] as $key => $val){
            if($x == 0){
                $equal = $val;
            }else{
                if($equal == $val){
                    $outletType = 'CO';
                }else{
                    $outletType = 'XO';
                }
            }
            $x++;
        }
		// Add a condition of Catch Basin Check by AM 											  
        if(count($inputs['lines']) == 1){
            $outletType = 'EO';
        } 
        
        $tempLines = array();
        foreach($inputs['lines'] as $key => $val){
            if(strpos($key, 'hor_') > -1){
                $tempLines['hor'][] = $val;
            }else {
                $tempLines['var'][] = $val;
            }
        }

        $inputs['lines'] = $tempLines;
        // \app\components\Utility::debug($inputs); exit;
        $frameTypeInput = $inputs['FeaturesAll']['fk_featuretype'];
        $flowrate = 0;
        if(isset($inputs['Quotations']['flow_rate']) && $inputs['Quotations']['flow_rate'] > 0){
            $flowrate = $inputs['Quotations']['flow_rate'];
        }
        
        if($frameTypeInput == 2 || $frameTypeInput == 3){
            $grateType = 'ductile_iron';
            $seriesId = 1;
            $manualSeries = 'DI';
            $seriesMain   = 'D';
        }else if($frameTypeInput == 1){
            $grateType = 'polypropylene';
            $seriesId = 2;
            $manualSeries = 'PP';
            $seriesMain   = 'P';
        }else{
            $seriesId = 3;
            $manualSeries = 'DX';
            $seriesMain   = 'DX';
        }

        $seriesInput = $inputs['FeaturesAll']['grate_material'];
        // Get Catch Basin Title and length by AM (Start)
            if(!empty($catch_basin_size))
            {
             $catchBasin = Catchbasin::find()->where(['catch_basin_size'=> $catch_basin_size,'trash_basket' => $trash_basket])->one();   
            }
            if (!empty($catchBasin)) {
                $cbTitle = $catchBasin->title;
                $cbLength = $catchBasin->length;
            }
            else
            {
                $cbTitle = '';
                $cbLength = '';
            }
        // Get Catch Basin Title and length by AM (End) 												 												
        /*if(strpos($seriesInput, 'D Series') > -1){
            
            
        }else if(strpos($seriesInput, 'P Series') > -1){
            
            
        }else{
            
            
        }*/
        $grateTypeGuard = @$inputs['Quotations']['frame_guard'];
        // echo $grateTypeGuard;exit;
        $grateShort = '';
        if(isset($grateTypeGuard) && $grateTypeGuard != ''){
            if($grateTypeGuard == 'fg'){
                $grateShort = '-FG';
            }else{
                $grateShort = '-FS';
            }
        }
       
        $flowRateData = \app\models\Flowratedata::find()->where(['and', 'gpm >= '.$flowrate, 'series_id = '.$seriesId])->one();

        $flowRateValue= '';
        if($flowRateData){
            $flowRateVExploded= explode('-', $flowRateData->product_no);
            $flowRateValue = $flowRateVExploded[1];
            // \app\components\Utility::debug($flowRateValue);exit;
        }

        $possibilities  = array();
        $drainsData     = array();
        $lengthSolution = array();
        $length         = 0;
        $x = $y         = 0;
        // exit;
        $length4          = 0;
        $totalLength = 0;
        $FL = 0;
        $flowRateMaxValue = 0;
        /*echo "<pre>";
        print_r($inputs);
        exit;*/
        if($flowrate>0){
            switch ($flowRateValue[1]) {
                case 'A':
                    $MaxSlopeValueInSeries = 20; 
                    break;
                case 'B':
                    $MaxSlopeValueInSeries = 40;
                    break;
                case 'C':
                    $MaxSlopeValueInSeries = 60;
                    break;
                case 'D':
                    $MaxSlopeValueInSeries = 80;
                    break;
                case 'E':
                    $MaxSlopeValueInSeries = 100;
                    break;
                
                default:
                    # code...
                    break;
            }
            $flowRateMaxValue   = 100 - ($MaxSlopeValueInSeries - (20-$flowRateValue[0]*4)) ;
            $flowRateMaxValueOrignal = $MaxSlopeValueInSeries - (20-$flowRateValue[0]*4);

        }


        

        foreach($inputs['lines'] as $dir => $lengths){

            foreach($lengths as $l){
                
                $maxLength         = $l;
                $l = $l.'-'.$x;
                // echo "<pre>";
                // print_r($lengthSolution);
                // exit;
                $result            = array();
                $drainsQuery       = \app\models\Drains::find();
                $drainsQuery->where(['series_id' => $seriesId]);
                
                $modLength          = $maxLength%4;
                $divider            = $maxLength/4;
                
                if($flowrate>0)
                {
                    if(($flowrate > 405 && $maxLength > 50) || ($flowrate < 405 && $maxLength > 50))
                    {
// Change this fix condtion due 56 to 60 parts length issue and flowrate 370 and 270 (Start)                        // Change this fix condtion due 56 to 60 parts length issue and flowrate 370 and 270 (Start) 
                        if($flowRateMaxValue > $maxLength)
                        {
                            if($flowRateMaxValue > $maxLength && $maxLength >= 56 && $maxLength <= 60)
                            {
                                // echo "YesC";
                                $drainsQuery->andWhere("title like '%1A'");
                            }
                            //}
                            else if ($flowRateMaxValue > $maxLength)
                            
                            // if($flowRateMaxValue > $maxLength)
                            {
                                // echo "Yes1";
                                $drainsQuery->andWhere("title like '%$flowRateValue'");
                            }
                        }
                        // Change this fix condtion due 56 to 60 parts length issue and flowrate 370 and 270  (End) 
                        else
                        {
                            if($flowRateMaxValueOrignal > $maxLength )
                            {
                                // echo "Yes2";
                                $drainsQuery->andWhere("title like '%$flowRateValue'");
                            }
                            else
                            {
                                // echo "Yes3";
                               $drainsQuery->andWhere("title like '%1A'"); 
                            }
                            
                        }
                        
                    }
                    else if(($flowrate > 405 && $maxLength < 50) || ($flowrate < 405 && $maxLength < 50))
                    { 
                        // echo "Yes4";
                        $drainsQuery->andWhere("title like '%$flowRateValue'");
                        
                    }
                    else if($flowrate <= 405 && $maxLength >= 50)
                    {
                        if($maxLength > 50)
                        {
                            // echo "Yes5";
                            $drainsQuery->andWhere("title like '%1A'");
                        }
                        else
                        {
                            // echo "Yes6";
                            $drainsQuery->andWhere("title like '%$flowRateValue'");
                        }
                    }
                    else if($flowrate >= 405 && $maxLength <= 50) 
                    {
                        if($maxLength < 50)
                        {
                            // echo "Yes7";
                             $drainsQuery->andWhere("title like '%$flowRateValue'");
                        }
                        else
                        {
                            // echo "Yes8";
                            $drainsQuery->andWhere("title like '%1A'");
                        }
                    }
                    
                }else{
                    if($modLength > 0 && $maxLength < 20)
                    {
                        $drainsQuery->andWhere("title like '%5A'");

                    }
                    else if($modLength == 0)
                    {
                        if($maxLength <= 20 || $maxLength > 36){// || $modLength > 0
                            $drainsQuery->andWhere("title like '%1A'");
                            $drainsQuery->andWhere("title not like '%_N%'");
                        }else{
                            if($divider > 6){
                                $drainsQuery->andWhere("title like '%1A%'");              
                            }else{
                                $drainsQuery->andWhere("title like '%AA%'");    
                            }
                        }
                    }else{
                        $drainsQuery->andWhere("title like '%1A'");
                    }
                }

                // add like statement here 
                $drains            =  $drainsQuery
                                      //->limit(20)
                                      ->all();

                // ob_start();
                // var_dump($drains);
                // $drains = ob_get_clean();
                
                // \app\components\Utility::debug($drains); exit;
                // ->createCommand()
                // ->getRawSql();
               /* \app\components\Utility::debug($seriesId); exit;*/
                
                set_time_limit(0);
                ini_set('memory_limit', '4000M');

                foreach($drains as $d){
                    $this->existingDrains[]         = $d->title;

                    $this->allNodes[$d->length][$d->title]   = 1;


                    /*$query          = "select d.*
                                       from drains d
                                       join drainproducts dp on d.id = dp.drain_id and dp.type = 'n'
                                       join drains d2 on d2.title = dp.product_code
                                       where d.title = '".$d->title."'
                                       ";*/

                    $query          = \app\models\Drains::find()
                                       ->leftJoin('drainproducts','drains.id=drainproducts.drain_id')
                                       ->where(['drains.title'=>$d->title]);
                                    
                    
                    // echo $query;exit;
                    /*$connection     = Yii::$app->getDb();
                    $command        = $connection->createCommand($query);
                    $result         = $command->queryAll();*/
                    $result         = $query->all();

                    $possibilities[$dir][$l][$d->title] = [];
                    $possibilities[$dir][$l][$d->title]['output'] = array();
                    // \app\components\Utility::debug($result);exit;
                    
                    $this->checkLength = 0;
                    $maxLength += 0;
                    // echo $maxLength;

                    do{
                        if($flowrate>0)
                        {
                            if(($flowrate > 405 && $maxLength > 50) || ($flowrate < 405 && $maxLength > 50) || ($flowrate < 405 && $maxLength < 50))
                            {
                                // Change this fix condtion due 56 to 60 parts length issue and flowrate 370 and 270  (Start)
                                if($flowRateMaxValue > $maxLength)
                                {
                                        if($flowRateMaxValue > $maxLength && $maxLength >= 56 && $maxLength <= 60)
                                        {
                                            // echo "part1";
                                            $this->getnextbasedonlength($maxLength, 'n');
                                        }
                                        else if ($flowRateMaxValue > $maxLength)
                                        {
                                            // echo "partC";
                                            $this->getnextbasedonlength($maxLength, 'n');
                                        }  
                                } 
                                
                                // Change this fix condtion due 56 to 60 parts length issue and flowrate 370 and 270  (End) 
                                else
                                {
                                    if($flowRateMaxValueOrignal > $maxLength )
                                    {
                                        // echo "part2";
                                        $this->getnextbasedonlength($maxLength, 'p');
                                    }
                                    else
                                    {
                                        // echo "part3";
                                       $this->getnextbasedonlength($maxLength, 'n');
                                    }
                                    
                                }
                            }
                            else if($flowrate > 405 && $maxLength < 50)
                            { 
                                $this->getnextbasedonlength($maxLength, 'p');
                            }
                            else if(($flowrate <= 405 && $maxLength >= 50) || ($flowrate >= 405 && $maxLength <= 50))
                            {
                                // echo "part4";
                                $this->getnextbasedonlength($maxLength, 'n');
                            }
                            else
                            {
                                //echo "part5";
                                $this->getnextbasedonlength($maxLength, 'n');
                            }
                        }
                        else
                        {
                            if($modLength > 0 && $maxLength < 20)
                            {
                                $this->getnextbasedonlength($maxLength, 'p');
                            }
                            else if($modLength > 0 && $maxLength > 20)
                            {
                                $this->getnextbasedonlength($maxLength, 'n');    
                            }
                            else if($this->checkLength < $maxLength) 
                            {
                                $this->getnextbasedonlength($maxLength, 'n');    
                            }
                        }
                        $y++;
                    }while($this->checkLength < $maxLength);
                    /*exit;*/
                   
                    
                    $possibilities[$dir][$l][$d->title]['packages']      = $this->allNodes;
                    $possibilities[$dir][$l][$d->title]['checkLength']   = $this->checkLength;
                        // \app\components\Utility::debug($possibilities);exit;
                    foreach($possibilities[$dir][$l][$d->title]['packages']['4'] as $pack => $val){
                        //Removed N48 completely on 72717 by removing '&& $modLength > 0' from If below;
                        if(strpos($pack, '_N48') > -1){
                            unset($possibilities[$dir][$l][$d->title]['packages']['4'][$pack]);
                        }                        
                    }

                    $newCheckLength      = 0;
                    $drainsArray         = array();
                    
                    // foreach($possibilities[$dir][$l][$d->title]['packages']['4'] as $key => $val){
                    //     if(($newCheckLength+4) <= $maxLength){
                    //         if(strpos($key, 'A') > -1){
                    //             $drainsArray[$key] = 1;
                    //             $newCheckLength += 4;    
                    //         }                            
                    //     }
                    // }

                    if($modLength == 1){
                        $drainsArray[$manualSeries.'_AB_N12'] = 1;
                        $newCheckLength += 1;
                    }else if($modLength == 2){
                        $drainsArray[$manualSeries.'_AB_N24'] = 1;
                        $newCheckLength += 2;
                    }else if($modLength == 3){
                        $drainsArray[$manualSeries.'_AB_N36'] = 1;
                        $newCheckLength += 3;
                    }

                    /*echo "<pre>";
                    print_r($possibilities);
                    exit;*/
                    $checkOthers = 0;
                    $arraykeys = array_keys($possibilities[$dir][$l][$d->title]['packages']['4']);

                    // if($maxLength <= 20 && $modLength > 0){
                    //     $possibilities[$dir][$l][$d->title]['packages']['4'] = array_reverse($possibilities[$dir][$l][$d->title]['packages']['4']);
                    // }
                    // else if($modLength == 0){
                    //     $possibilities[$dir][$l][$d->title]['packages']['4'] = array_reverse($possibilities[$dir][$l][$d->title]['packages']['4']);
                    // }
                    // else if($x == 1 && $modLength == 0){
                    //     $possibilities[$dir][$l][$d->title]['packages']['4'] = array_reverse($possibilities[$dir][$l][$d->title]['packages']['4']);
                    // }
                    foreach($possibilities[$dir][$l][$d->title]['packages']['4'] as $key => $val){
                        if(($newCheckLength+4) <= $maxLength){
                            // if(strpos($key, 'A') === false){
                                $checkOthers++;
                                $drainsArray[$key] = 1;
                                $newCheckLength += 4;    
                            // }                            
                        }
                    }
                    // if($checkOthers == 0){
                        
                    // }
                    // \app\components\Utility::debug($drainsArray);exit;
                    if(isset($inputs['Quotations']['flow_rate']) && !empty($inputs['Quotations']['flow_rate'])){
                        
                        $lengthSolution['packages'][$dir][$l] = $drainsArray;
                    }else{
                        $lengthSolution['packages'][$dir][$l] = array_reverse($drainsArray);
                    }
                    
                    $FL += count($drainsArray)-1;

                }
                $totalLength = $totalLength + $l;
                $x++;
            }
        }
        $lengthSolution['outletPosition'] = $outletPosition;
        // \app\components\Utility::debug($possibilities);
        // \app\components\Utility::debug($possibilities[$dir][$l][$d->title]['packages']);
        // \app\components\Utility::debug($lengthSolution); exit;
        set_time_limit(30);
        //getting channels and frames
        $channelsCount  = $frameCount = 0;
        foreach($lengthSolution['packages'] as $dir=>$values){
            foreach($values as $key => $p){
                // \app\components\Utility::debug($key);
                // \app\components\Utility::debug($p);
                // exit;
                $query          = \app\models\Components::find()->select(['components.*', 'drains.title']);
                $query->innerJoin('drains', "components.drain_id = drains.id and drains.title in ('".implode("','", array_keys($p))."')");

                $resultData     = $query->asArray()->all();


                foreach($resultData as $rs){
                    $quantityDrain = $lengthSolution['packages'][$dir][$key][$rs['title']];

                    if(strpos($rs['product_code'], 'C-') > -1){
                        $channelsCount  = $channelsCount + ($quantityDrain * $rs['quantity']);
                    }else{
                        $frameCount     = $frameCount + ($quantityDrain * $rs['quantity']);
                    }
                }
            }
        }
        // echo $channelsCount.'<br>';
        // echo $frameCount;exit;
        // getting fixtures
        $ruleHardware   = \app\models\Graterules::find()
                                               ->where([ 'series_id' => $seriesId, 
                                                         'rule_type' => 'hardware'])
                                               ->andWhere(['grate_type' => 
                                                                    ['grate_type', $grateType, 
                                                                     'grate_type', $grateTypeGuard]
                                                         ])
                                               ->all();
        // \app\components\Utility::debug($ruleHardware);exit;
        $num4Grates = $num3Grates = $num2Grates = $num1Grates = 0;
        foreach($lengthSolution['packages'] as $len=>$values){
            foreach($values as $len){
                foreach($len as $key => $val){
                    if(strpos($key, '_N12') > -1){
                        $num1Grates++;
                    }else if(strpos($key, '_N24') > -1){
                        $num2Grates++;
                    }else if(strpos($key, '_N36') > -1){
                        $num3Grates++;
                    }else if(strpos($key, '_N48') > -1){
                        $num4Grates++;
                    }else{
                        $num4Grates++;
                    }
                }    
            }
        }
        $totalNoOfDrains = $num4Grates + $num3Grates + $num2Grates + $num1Grates;
        // echo $num4Grates;
        // echo $num3Grates;exit;
        foreach($ruleHardware as $rule){
            $type = $rule->rule_type;
            switch($rule->hardware_code){
                // case 'DL-H5':
                //     $hRuleQty   = $length4 * $rule->termmultiplier;
                //     break;
                // case 'DL-H4':
                //     $hRuleQty   = $totalNoOfDrains;
                //     break;
                case 'DL-H4':
                case 'DL-H5':
                case 'DL-H6':
                case 'DLX-H9':
                case 'DLX-H10':
                    $hRuleQty = (1 * $num1Grates) + (1 * $num2Grates) + (2 * $num3Grates) + (2 * $num4Grates);
                    break;
                case 'DLX-H8':
                    $hRuleQty = (2 * $num1Grates) + (4 * $num2Grates) + (4 * $num3Grates) + (4 * $num4Grates);
                    break;
                case 'H-2920':
                    $hRuleQty   = $totalNoOfDrains;
                    break;
                case 'FG-CC':
                case 'FGX-CC':
                    $hRuleQty = 0;
                    foreach($inputs['lines'] as $dir=>$values){
                        foreach($values as $ili){
                            $hRuleQty   = $hRuleQty + $this->roundUp($ili * $rule->termmultiplier, 0);
                        }    
                    }                
                    break;
                case 'DL-H1':         
                    $hRuleQty   = $channelsCount * $rule->termmultiplier;
                    break;
                case 'DL-H2':
                    $hRuleQty   = $frameCount * $rule->termmultiplier;
                    break;
                case 'F-L':
                    /*$hRuleQty = 0;
                    foreach($inputs['lines'] as $dir=>$values){
                        foreach($values as $ili){
                            $hRuleQty   = $hRuleQty + $this->roundUp($ili * $rule->termmultiplier, 0);
                        }
                    }*/
                    $hRuleQty = $FL;
                    break;
            }
            $condition  = ['parts.status' => 1];
            $query      = \app\models\Parts::find()->where($condition);
            $query->andFilterWhere(
                ['or', 
                    ['LIKE', 'parts.code', $rule->hardware_code],
                ]
            );
            $partResult = $query->one();
            $keys  = @array_keys($lengthSolution['fixtures']);
            
            if(isset($partResult->code) && !empty($partResult->code)){
                $showPart = $partResult->code;
            }else{
                $showPart = $rule->hardware_code;
            }
            $lengthSolution['fixtures'][$rule->hardware_code] = [
                                        'part_code'       => $showPart,
                                        'quantity'        => @$hRuleQty,
                                        ];
        }
        $ruleGrates = \app\models\Graterules::find()
                                      ->where([ 'series_id' => $seriesId, 
                                                'rule_type' => 'guard',
                                                'grate_type'=> $grateTypeGuard
                                                ])
                                      ->all();

        foreach($ruleGrates as $rule){
            $type = $rule->rule_type;
            switch($rule->hardware_code){
                 case 'FG-GV':
                 case 'FG-SS':
                    $hRuleQty = 0;
                    foreach($inputs['lines'] as $dir=>$values){
                        foreach($values as $ili){
                            $hRuleQty   = $hRuleQty + $this->roundUp($ili * $rule->termmultiplier, 0);
                        }    
                    }    
                    break;
            }
            $condition  = ['parts.status' => 1];
            $query      = \app\models\Parts::find()->where($condition);
            $query->andFilterWhere(
                ['or', 
                    ['LIKE', 'parts.code', $rule->hardware_code],
                ]
            );
            $partResult = $query->one();
            $lengthSolution['fixtures'][$rule->hardware_code] = [
                                        'part_code'       => $partResult->code,
                                        'quantity'        => $hRuleQty,
                                        ];
        }

        // some hardcoded rules as per given document file on 22-Jun-2017
        $rules['EC-M']  = 1; // only one part which will be at bottom/end of the drain
        $rules['EC-F']  = 1; // only one part which will be at top/start of the drain
        // Rule for dispalying Catch basin by AM (Start)
        if(!empty($catch_basin_size) && $trash_basket == 'no')
        {

            $cb = 'CB-';
            $size = filter_var($cbTitle, FILTER_SANITIZE_NUMBER_INT);
            $t = '';

        }
        elseif(!empty($catch_basin_size) && $trash_basket == 'yes')
        {
            $cb = 'CB-';
            $size = filter_var($cbTitle, FILTER_SANITIZE_NUMBER_INT);
            $t = 'T';
        }
        if(!empty($catch_basin_size))
        {
            $cbRuleRaw = $cb.$size.'24'.$t;
            $cbRule = $cbRuleRaw.' Catch Basin w/DI Grate';
            $cbRuleWithT = $cbRuleRaw.' Catch Basin w/DI Grate- Trash Basket';
        }   
         if(!empty($t) && !empty($catch_basin_size))
            {
                $rules[$cbRuleWithT]  = 1; 
            }
        elseif(!empty($catch_basin_size))
            {
                $rules[$cbRule]  = 1;   
            }
            // Rule for dispalying Catch basin by AM (End)
        foreach($rules as $key => $val){
            $lengthSolution['fixtures'][$key] = [
                                        'part_code'       => $key,
                                        'quantity'        => $val,
                                        ];
        }

        $lengthSolution['lengths']  = $lines;
        $lengthSolution['nodes']    = $inputs['nodes'];
        // \app\components\Utility::debug($lengthSolution);exit;
        //printing values as per needed
        $bestsolution = '';
        $x = 0;
        foreach($lengthSolution['packages'] as $key => $val){
            $output[] = array('', $key, $val);
        }
        foreach($lengthSolution['fixtures'] as $ph){
            $output[] = array($ph['part_code'], '', $ph['quantity']);
        }
		//Change of out Put for Catch Basin by AM (Start)
        if($catch_basin_size =='')
        {
            $output['outletPosition'] = $outletPosition;
        }
        else
        {
            $output['catchBasinPosition'] = $outletPosition;
        }
        //$output['outletPosition'] = $outletPosition;
        //Change of out Put for Catch Basin by AM (End)												 						   
        $bestSolution = json_encode($output);
        // $bestSolution = $output;
        $x++;
        // \app\components\Utility::debug($output);exit;
        
        /*$sTrimmed       = trim($seriesInput);
        $seriesValue    = trim($sTrimmed[0].$sTrimmed[1]);*/

        /*$gratetype = Yii::$app->request->post('gratetype');
        if($gratetype == 'stainless_steel')
        {
            $frameType = '-FS';
        }
        elseif($gratetype == 'galvanized_steel')
        {
            $frameType = '-FG';
        }
        else
        {
            $frameType = '';
        }*/
        // \app\components\Utility::debug($this->checkLength);
        // \app\components\Utility::debug($output);exit;
        // \app\components\Utility::debug($gratetype); exit;
        $seriesValue    = trim($seriesInput);
		//Change of Drain Model array for uper Title by AM (Start)
		$cb = '-CB';
        $size = filter_var($cbTitle, FILTER_SANITIZE_NUMBER_INT);
        if($trash_basket =='yes'){
            $t = 'T';
        }
        else
        {
            $t = '';
        }
        $basinTitle = $cb.'-'.$size.'24'.$t;
        if($catch_basin_size !='')
        {
            $totalLength_cb = $totalLength + $cbLength;
        }
        if($catch_basin_size =='')
        {
         $drainModel[]   = $seriesMain.$totalLength.'-'.$outletType.'-'.$seriesValue.$grateShort;  
        }
        else
        {
        $drainModel[]   = $seriesMain.$totalLength.'-'.$outletType.'-'.$seriesValue.$grateShort.$basinTitle;
        }
        //Change of Drain Model array for uper Title by AM (End)
        //$drainModel[]   = $seriesMain.$totalLength.'-'.$outletType.'-'.$seriesValue.'-'.$grateShort;														  													 
        // \app\components\Utility::debug(json_encode($output));
        // \app\components\Utility::debug($output); exit;
        // exit;
        //save best solution 
        /*echo "<pre>";
        print_r($inputs);
        print_r(Yii::$app->request->post());
        exit;*/
        $quotation              = new Quotations();
        $quotation->job_id      = $jobID;
        $quotation->details     = $bestSolution;
        $quotation->status_id   = 4;
        $quotation->drain_model = json_encode($drainModel);
        $quotation->label       = $inputs['Quotations']['label'];
        $quotation->created_by  = Yii::$app->user->getId();
        $quotation->create_date = date('Y-m-d H:i:s');
        $quotation->status      = '0';
        if($quotation->save()){
            $quoteId = $quotation->id;
            return $this->redirect(['view','id'=>$quoteId]);
        }else{
            echo "<pre>";
            print_r($quotation->getErrors());
            exit;
        }
    }
     public function getNextData($drainTitle){
        $result     = array();
        $drain      = \app\models\Drains::find()
                                      ->where(['title' => $drainTitle])
                                      ->joinWith('drainproducts')
                                      ->one();

        $result['settings']['length']     = $drain->length;
        $result['settings']['title']      = $drain->title;

        foreach($drain->drainproducts as $prods){
            if($prods->type == 'n'){
                $result['products']['n'][] = $prods->product_code;    
            }else{
                $result['products']['p'][] = $prods->product_code;    
            }                
        }
        
        return $result;
    }

    public function getNextBasedOnLength($maxLength, $type){
        $title = $this->existingDrains[count($this->existingDrains)-1];
        $modLength = $maxLength % 4;
        $nodeNextData   = $this->getNextData($title);

        if($nodeNextData['settings']['length'] == '4'){
            if(isset($nodeNextData['products'][$type])){
                foreach($nodeNextData['products'][$type] as $key => $next){
                    if($type == 'n' && $maxLength > 24 && strpos($title, '_5A') === false){
                        $tempKeys = array_keys($this->allNodes[4]);
                        $nextData = $this->getNextData($tempKeys[0]); 
                    }else{
                        $nextData = $this->getNextData($next);  
                    }
                    
                    if(strpos($next, '_N1') == false && strpos($next, '_N2') === false && strpos($next, '_N3') === false){ 

                        if(!in_array($next, $this->existingDrains)){  
                            $this->existingDrains[] = $next;
                            $this->allNodes[$nextData['settings']['length']][$next] = 1;
                        } else {
                            if(isset($nextData['products']['n'])){
                                foreach($nextData['products']['n'] as $ntemp){
                                    // \app\components\Utility::debug($ntemp);
                                    if($maxLength % 4 == 0){
                                        $this->allNodes[$nextData['settings']['length']][$ntemp] = 1;
                                    }else{
                                        $nTitle = $nextData['products']['n'][count($nextData['products']['n'])-1];
                                        $nextData = $this->getNextData($nTitle);
                                        $nextDrain = $nextData['products']['n'][count($nextData['products']['n'])-1];

                                        $this->allNodes[$nextData['settings']['length']][$nextDrain] = 1;
                                    }
                                }
                            }
                        }
                        if($key==0){
                            $this->checkLength = $this->checkLength + $nextData['settings']['length']; 
                        }
                    }
                        
                } 
                return true;
            }else{
                //If next part is empty
                $this->checkLength = $this->checkLength + 4;
            }
        }
    }

    public function actionSubmitapproval($recordId){
        $record     = Quotations::findOne($recordId);
        $record->status = 1;
        $record->save();

        $notificationText           = $this->settings['notifyAdminQuoteCreated'];
        $notificationText           = '<a href="'.Url::home(true).'quotations/viewquote?recordId='.$recordId.'" target="_blank">'.$notificationText.'</a>';
        $notification               = new \app\models\Notification();
        $notification->userrole_id  = 3;
        $notification->notificationtext=$notificationText;
        $notification->is_read      = 0;
        $notification->create_date  = date('Y-m-d H:i:s');
        $notification->created_by   = Yii::$app->user->getId();
        $notification->status       = 1;
        $notification->save();

        Yii::$app->getSession()->setFlash('success', 'Configuration has been submitted for approval !');
        return $this->redirect(['/quotations/viewquote/?recordId='.$recordId]); 
    }

    public function roundUp($value, $places) 
    {
        $mult = pow(10, abs($places)); 
         return $places < 0 ?
        ceil($value / $mult) * $mult :
            ceil($value * $mult) / $mult;
    }


     public function actionGeneratepdf($recordId){
        $pathp = \Yii::getAlias('@webroot')."/configurations/";
        $img = $_POST['imgBase64'];
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $fileData = base64_decode($img);
        $fileName = $pathp.$recordId.'.png';
        file_put_contents($fileName, $fileData);

        $quoteData      = Quotations::findOne($recordId);
        $path = \Yii::getAlias('@webroot')."/quotes/";

        $content = $this->renderPartial('pdf-view', 
                                       [
                                            'quoteData' => $quoteData,
                                            'customCss' => '.form-actions, h1{display:none;}',
                                        ]
                                        );
        $content = str_replace('{{canvas_photo_here}}', '<img src="'.$fileName.'">', $content);
        // echo $content;exit;
        $header = '<div class="logo"><img src="'.\Yii::getAlias('@webroot').'/images/logo.png" width="200"></div><div class="slogan"><div class="static-black">Selection Process</div></div> ';
        $footer = '<div class="left-side-pdf">Quotation generated at : <b>'. date('d-M-Y H:i:s') .'</b></div><div class="right-side-pdf">&copy; copyright ATS</div><div class="clear"></div>';

        $filename   = $recordId.'-'.strtotime(date('d-M-Y')).'.pdf';
        $mpdf       =   new mPDF('c', 'A4', '', '', '15', '15', '30', '30 ');
        $mpdf->SetHeader($header);
        $mpdf->SetFooter($footer);
        $mpdf->WriteHtml($content);
        $mpdf->Output($path.$filename,'F');

        Yii::$app->getSession()->setFlash('success', 'PDF generated Successfully !');
        $quoteData->filename = $filename;
        $quoteData->save();
        exit;
    }  


    /**
     * Updates an existing Quotations model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Quotations model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Quotations model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Quotations the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Quotations::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //Get Grate Material
    public function actionGetGrateMaterial() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $trench_frame_id = $parents[0];
                $grate_ada_id = $parents[1];
                $grate_heel_proof_id = $parents[2];
                $out = self::getGrateMaterialList($trench_frame_id,$grate_ada_id,$grate_heel_proof_id); 
                echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }
    public static function getGrateMaterialList($trench_frame_id,$grate_ada_id,$grate_heel_proof_id){
        $data= \app\models\FeaturesAll::find()
           ->where(['fk_featuretype'=>$trench_frame_id,'grate_ada'=>$grate_ada_id,'grate_heel_proof'=>$grate_heel_proof_id])
           ->select(['suffix As id','grate_material AS name' ])->asArray()->all();
        return $data;
    }

    //Get Load Rating
    public function actionGetLoadRating() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                
                $trench_frame_id = $parents[0];
                $grate_ada_id = $parents[1];
                $grate_heel_proof_id = $parents[2];
                $grate_material_id = $parents[3];
                $out = self::getGetLoadRatingList($grate_material_id,$trench_frame_id,$grate_ada_id,$grate_heel_proof_id); 
                echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }
    public static function getGetLoadRatingList($grate_material_id,$trench_frame_id,$grate_ada_id,$grate_heel_proof_id){
        $data= \app\models\FeaturesAll::find()
           ->where(['suffix'=>$grate_material_id,'fk_featuretype'=>$trench_frame_id,'grate_ada'=>$grate_ada_id,'grate_heel_proof'=>$grate_heel_proof_id])
           ->select(['grate_load_rating as id','grate_load_rating AS name' ])->asArray()->all();
        return $data;
    }
}
