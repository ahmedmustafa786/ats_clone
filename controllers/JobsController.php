<?php

namespace app\controllers;

use Yii;
use app\models\Jobs;
use app\models\JobsSearch;
use app\models\Quotations;
use app\models\QuotationsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\UploadedFile;

/**
 * JobsController implements the CRUD actions for Jobs model.
 */
class JobsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Jobs models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new JobsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Jobs model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $searchModelQuotation = new QuotationsSearch();
        $queryParams = array_merge(array(),Yii::$app->request->getQueryParams());
        $queryParams['QuotationsSearch']['job_id']=$id;
        $dataProviderQuotation = $searchModelQuotation->search($queryParams);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModelQuotation' => $searchModelQuotation,
            'dataProviderQuotation' => $dataProviderQuotation,
        ]);
    }

    /**
     * Creates a new Jobs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Jobs();
        $buildingClass= \app\models\Features::find()
                        ->where(['featuretype_id' => '4'])
                        ->all();
        if ($model->load(Yii::$app->request->post()) ) {
            if($_FILES['Jobs']['name']['file'] != '')
            {
                $path                       = \Yii::getAlias('@webroot')."/projects/";
                $model->file        = UploadedFile::getInstance($model, 'file');
                $model->file->saveAs($path . str_replace(" ", "-", strtolower($model->file->baseName)) . '.' . strtolower($model->file->extension));
                $model->file = str_replace(" ", "-", strtolower($model->file->name));
            }
            if($model->save()){
                return $this->redirect(['view','id'=>$model->id]);
            }else{
               return $this->render('create', [
                    'model' => $model,
                    'buildingClass'=>$buildingClass,
                ]); 
            }
            
        } else {
            return $this->render('create', [
                'model' => $model,
                'buildingClass'=>$buildingClass,
            ]);
        }
    }

    /**
     * Updates an existing Jobs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $buildingClass= \app\models\Features::find()
                        ->where(['featuretype_id' => '4'])
                        ->all();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'buildingClass'=>$buildingClass,
            ]);
        }
    }

    /**
     * Deletes an existing Jobs model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    public function actionSubStates() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $country_id = $parents[0];
                $out = self::getStatesList($country_id); 
                // the getSubCatList function will query the database based on the
                // cat_id and return an array like below:
                // [
                //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
                //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
                // ]
                echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }
    public static function getStatesList($country_id){
        $data= \app\models\States::find()
           ->where(['country_id'=>$country_id])
           ->select(['id','title AS name' ])->asArray()->all();
        return $data;
    }
     
    public function actionSubCities() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            
            if ($parents != null) {
                $state_id = $parents[0];
                $data = self::getCititesList($state_id);
                /**
                 * the getProdList function will query the database based on the
                 * cat_id and sub_cat_id and return an array like below:
                 *  [
                 *      'out'=>[
                 *          ['id'=>'<prod-id-1>', 'name'=>'<prod-name1>'],
                 *          ['id'=>'<prod_id_2>', 'name'=>'<prod-name2>']
                 *       ],
                 *       'selected'=>'<prod-id-1>'
                 *  ]
                 */
               
               echo Json::encode(['output'=>$data, 'selected'=>'']);
               return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }

    public static function getCititesList($state_id){
        $data= \app\models\Cities::find()
           ->where(['state_id'=>$state_id])
           ->select(['id','title AS name' ])->asArray()->all();
        return $data;
    }

    /**
     * Finds the Jobs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Jobs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Jobs::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
