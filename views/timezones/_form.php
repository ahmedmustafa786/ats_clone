<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Timezones */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="timezones-form">
  <?php $form = ActiveForm::begin(); ?>
  <?= $form->field($model, 'country_id')->textInput() ?>
  <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
  <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>
  <div class="form-actions">
    <div class="form-group">
      <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
    </div>
  </div>
  <?php ActiveForm::end(); ?>
</div>
