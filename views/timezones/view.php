<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Timezones */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Timezones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
  <div class="row">
    <h1 class="col-lg-4">
      <?= Html::encode($this->title) ?>
    </h1>
    <div class="col-lg-8">
      <div class="pull-right">
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
      </div>
    </div>
  </div>
</section>
<section class="content">
  <div class="timezones-view table-outer">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'country_id',
            'title',
            'status',
        ],
    ]) ?>
  </div>
</section>
