<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\Quotations */
/* @var $form yii\widgets\ActiveForm */
$this->registerJs("
    $(document).ready(function(){ 
        $('[type=hidden]:not([name=_csrf])').val('');
        $('[type=reset]').click(function(){
          $('div').removeClass('ac');
          $('.gline').html('');
          $('.gbox').html('');
          $('[type=hidden]:not([name=_csrf])').val('');
        });

        $('.grid' ).delegate('.gline', 'click', function() {
          $('.gbox-nav').addClass('hide');
          var offset  = $(this).offset();
          var left  = offset.left;
          var top   = offset.top;
          var classes= $(this).attr('class').split(' ');
          var selectedClass = classes[1];
          var nTop = 0;
          var nLeft= 0;
          console.log(selectedClass);
          if(selectedClass.search('-hor') > 0){
            nTop = top - 90;
            nLeft= left + 20;
          }else{
            nTop = top;
            nLeft= left + 20;
          }
          var id = $(this).parent().attr('id');
          var val = $(this).html();
          $('.gline-box').css('top', nTop);
          $('.gline-box').css('left', nLeft);
          if(val.length > 0){
            $('.gline-box input[name=length]').val(val.replace('ft', ''));
          }else{
            $('.gline-box input[name=length]').val('');
          }
          $('.gline-box').removeClass('hide');
          $('.gline-box input[type=number]').focus();
          $('.gline-box').attr('id', id);
          $('.gline-box').addClass(selectedClass+'-placement');
        });

        $('.gline-box').delegate('input[type=button]', 'click', function(){
          var id = $(this).parent().parent().attr('id');
          var classes = $(this).parent().parent().attr('class').split(' ');
          var selClass = classes[1].replace('-placement', '');

          $(this).parent().parent().removeClass(classes[1]);
          var value = $(this).parent().find('input[name=length]').val();
          

          if(value != ''){
            
            $('.gline-box').addClass('hide');
            $('#'+id+' .gline.'+selClass).html('');
            $('#'+id+' .gline.'+selClass).html('<div class=\'lenght_val\'>'+value+'ft</div>');

            if(selClass.search('-hor') > -1){
              $('#'+id+' .'+selClass).css('text-align', 'center');  
              $('input[name=lines_hor_'+id+']').val(value);
            }else{
              $('#'+id+' .'+selClass).css('padding-top', '60px');
              $('#'+id+' .'+selClass).css('padding-left', '8px');
              $('input[name=lines_var_'+id+']').val(value);
            }
            
            $('#'+id+' .'+selClass).addClass( 'ac');
          }else{
            $('#'+id+' .gline').html('');
            $('.gline-box').addClass('hide');
          }
        });

        $('.sidebar-toggle').click(function() {
          if($('.main-sidebar').hasClass('grid-sidebar')) {
            $('.main-sidebar').removeClass('grid-sidebar');
          } else {
            $('.main-sidebar').addClass('grid-sidebar');
          }
        });

        $('.grid').delegate('.gbox', 'click', function(){
          $('.gline-box').addClass('hide');
          var offset  = $(this).offset();
          var id = $(this).parent().attr('id');
          var left  = offset.left+80;
          var top   = offset.top;
          $('.gbox-nav').css('top', top);
          $('.gbox-nav').css('left', left);
          $('.gbox-nav').attr('id', id);
          $('.gbox-nav').removeClass('hide');
        });

        $('.gbox-nav').delegate('li', 'click', function(){
          var id  = $(this).parent().parent().attr('id');
          var text = $(this).find('img').attr('src');
          $('#'+id+' .gbox').html('<img src=\"'+text+'\" style=\"width:30px; height:30px; margin:20px;\">');
          $('input[name=nodes_'+id+']').val(text);
          $('.gbox-nav').addClass('hide');
        });

        // var dirty_bit = document.getElementById('page_is_dirty');
        // if (dirty_bit.value == '1') window.location.reload();
        // function mark_page_dirty() {
        //     dirty_bit.value = '1';
        // }


          // Show Catch Basin when click on Catch Basin
          $('#catch_basin').hide();
          $('#catch-basin-selected').on('click', function() {
            $('#catch_basin').slideToggle('slow');
          });

          // Hide Catch Basin when click on Outlet
          $('#catch_basin').hide();
          $('#outlet').on('click', function() {
            $('#catch_basin').hide('slow');
          });
          
          // Hide Grate Material and Loading on no data found 
          $('#error-form').hide();
         $('#grate_material-id').on('depdrop:change', function(event, id, value, count, textStatus, jqXHR) {
            if(count==0)
            {
              $('#grate_material_a').hide('slow');
              $('#submit').attr('disabled', 'disabled');
              $('#error-form').show();
              $('#error-form').html('No data found as per selection');
            }
            else
            {
              $('#grate_material_a').show('slow');
              $('#submit').removeAttr('disabled');
              $('#error-form').hide();
              $('#error-form').html('');
             
            }
        });
    });
    ",yii\web\View::POS_READY);
?>
<?php 
$form = ActiveForm::begin([
   'id' => 'quotaition-form',
   'action' => ['create','jobid'=>Yii::$app->request->get('jobid')]
]);
?>
<section>
  <div class="grid-selection">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <ul>
            <li>LEGEND</li>
            <li><img src="<?php echo Url::home(true);?>images/trench-unselected.svg">
              <p>Trench Unselected</p>
            </li>
            <li><img src="<?php echo Url::home(true);?>images/trench-selected.svg">
              <p>Trench Selected</p>
            </li>
            <li><img src="<?php echo Url::home(true);?>images/outlet.svg" class="outlet-icon">
              <p>Outlet</p>
            </li>
            <li><img src="<?php echo Url::home(true);?>images/unselected.svg" class="tee-icon">
              <p>Unselected</p>
            </li>
            <li><img src="<?php echo Url::home(true);?>images/catch-basin-selected.svg" class="tee-icon">
              <p>Catch Basin Selected</p>
            </li>
            <!-- 
            <li><img src="<?php echo Url::home(true);?>images/90-selected.svg" class="tee-icon">
              <p>90 Selected</p>
            </li>
            <li><img src="<?php echo Url::home(true);?>images/tee-selected.svg" class="tee-icon">
              <p>Tee Selected</p>
            </li>
            <li><img src="<?php echo Url::home(true);?>images/4-way-tee selected.svg" class="tee-icon">
              <p>4-Way Tee Selected</p>
            </li>
            -->
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-8">
          <div style="overflow:scroll; height:800px; width:100%">
            <?php $currentUrl = Yii::$app->request->getUrl();?>
              <?php if(strpos($currentUrl, 'addquote') > -1){ ?>
                <?php if(Yii::$app->session->getFlash('success')){ ?>
                  <div class="alert alert-success">
                      <?= Yii::$app->session->getFlash('success'); ?>  
                    </div>
                <?php }else if(Yii::$app->session->getFlash('error')){ ?>
                  <div class="alert alert-danger">
                      <?= Yii::$app->session->getFlash('error'); ?>  
                    </div>
                <?php } ?>
              <?php } ?>
            <div class="grid-autor" id="draggable">
              
              <div class="grid">
                <?php for($x = 1; $x <= 5; $x++){ ?>
                  <?php for($y = 1; $y <= 5; $y++){ ?>
                  <input type="hidden" name="nodes_<?php echo $x;?>_<?php echo $y;?>">
                  <input type="hidden" name="lines_hor_<?php echo $x;?>_<?php echo $y;?>">
                  <input type="hidden" name="lines_var_<?php echo $x;?>_<?php echo $y;?>">
                    <?php if($x == 5
                            && $y == 5){ ?>
                    <div class="grid-col" id="<?php echo $x;?>_<?php echo $y;?>">
                      <div class="gbox"></div>
                    </div>
                    <?php }else if($y % 5 == 0){ ?>
                    <div class="grid-col" id="<?php echo $x;?>_<?php echo $y;?>">
                      <div class="gbox"></div>
                      <div class="gline gline-var gl-bottom"></div>
                      

                    </div>
                    <?php }else if($x % 5 == 0){ ?>
                    <div class="grid-col" id="<?php echo $x;?>_<?php echo $y;?>">
                      <div class="gbox"></div>
                      <div class="gline gline-hor gl-right"></div>
                      
                    </div>
                    <?php }else{ ?>
                    <div class="grid-col" id="<?php echo $x;?>_<?php echo $y;?>">
                      <div class="gbox"></div>
                      <div class="gline gline-hor gl-right"></div>
                      <div class="gline gline-var gl-bottom"></div>
                    </div>
                    <?php } ?>
                  <?php } ?>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 grid-right">
        	<h2>QUALIFYING QUESTIONS</h2>
            <div class="grid-right-form form-inline">
            	<div class="row">
                	<div class="col-sm-6">
                    	<label>Trench Frame & Width</label>
                    </div>
                    <div class="col-sm-6 grid-form-option">
                    	<?= $form->field($modelFeature, 'fk_featuretype')->dropDownList($modelFeatures, ['id'=>'trench_frame-id', 'prompt' => 'Select ...'])->label(false); 
						$modelFeature->grate_ada = 'yes';
						?>
                    </div>
                </div>
                <div class="row">
                	<div class="col-sm-6">
                    	<label>Grate ADA Compliant</label>
                    </div>
                    <div class="col-sm-6 grid-form-option">
                    	<?= $form->field($modelFeature, 'grate_ada')->dropDownList([ 'yes' => 'Yes', 'no' => 'No', ],['id'=>'grate_ada-id'])->label(false);
						$modelFeature->grate_heel_proof = 'yes';
						?>
                    </div>
                </div>
                <div class="row">
                	<div class="col-sm-6">
                    	<label>Grate Heel Proof</label>
                    </div>
                    <div class="col-sm-6 grid-form-option">
                    	<?= $form->field($modelFeature, 'grate_heel_proof')->dropDownList([ 'yes' => 'Yes', 'no' => 'No', ],['id'=>'grate_heel_proof-id'])->label(false); ?>
                      <div id="error-form"></div>
                    </div>
                </div>
                <div id="grate_material_a">
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Grate Material</label>
                        </div>
                        <div class="col-sm-6 grid-form-option">
                            <?php
							echo $form->field($modelFeature, 'grate_material')->widget(DepDrop::classname(), [
									'options'=>['id'=>'grate_material-id'],
									'pluginOptions'=>[
										'depends'=>['trench_frame-id','grate_ada-id','grate_heel_proof-id'],
										'placeholder'=>'Select ...',
										'url'=>Url::to(['get-grate-material'])
									]
								])->label(false); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Grate Load Rating</label>
                        </div>
                        <div class="col-sm-6 grid-form-option">
                            <?php
							echo $form->field($modelFeature, 'grate_load_rating')->widget(DepDrop::classname(), [
								'options'=>['id'=>'grate_load_rating-id'],
								'pluginOptions'=>[
									'depends'=>['trench_frame-id','grate_ada-id','grate_heel_proof-id','grate_material-id'],
									'placeholder'=>'Select ...',
									'url'=>Url::to(['get-load-rating'])
								]
							])->label(false); ?>
                        </div>
                    </div>
                </div>
				
                <div id="catch_basin">
                	<div class="row">
                    	<div class="col-sm-6">
                        	<label>Catch Basin Size</label>
                        </div>
                        <div class="col-sm-6 grid-form-option">
                        	<?=
							 $form->field($modelCatchbasin, 'catch_basin_size')->widget(DepDrop::classname(), [
								'options'=>['id'=>'catch-basin-size-id'],
								'pluginOptions'=>[
								  'depends'=>['trench_frame-id'],
								  'placeholder'=>'Select ...',
								  'url'=>Url::to(['get-catch-basin-size'])
								]
							  ])->label(false);
					
							// $form->field($modelCatchbasin, 'catch_basin_size')->dropDownList($modelCatchbasinsize , ['id'=>'catch_basin_size', 'prompt' => 'Select ...']);
							?>
                        </div>
                    </div>
                    <div class="row">
                    	<div class="col-sm-6">
                        	<label>Trash Basket</label>
                        </div>
                        <div class="col-sm-6 grid-form-option">
                        	<?= $form->field($modelCatchbasin, 'trash_basket')->dropDownList(['yes' => 'Yes', 'no' => 'No', ],['id'=>'trash_basket', 'prompt' => 'Select ...'])->label(false);?>
                        </div>
                    </div>
                </div>
				<div class="row">
                	<div class="col-sm-6">
                    	<label>Frame Guard</label>
                    </div>
                    <div class="col-sm-6 grid-form-option">
                    	<?= $form->field($model, 'frame_guard')->radioList(['fs' => 'STAINLESS', 'fg' => 'GALVANIZED'], ['id'=>'frame_guard-id'])->label(false);?>
                    </div>
                </div>
                <div class="row">
                	<div class="col-sm-6">
                    	<label>Flow Rate</label>
                    </div>
                    <div class="col-sm-6 grid-form-option">
                    	<?= $form->field($model, 'flow_rate')->textInput(['type'=>'number'])->label(false);?>
                    </div>
                </div>
                <span class="not-required-text">Flow Rate must be between 225 - 610</span>
                <div class="row">
                	<div class="col-sm-6">
                    	<label>Configuration Label</label>
                    </div>
                    <div class="col-sm-6 grid-form-option">
                    	<?= $form->field($model, 'label')->textInput()->label(false);?>
                    </div>
                </div>
                <div class="row">
                	<div class="col-sm-6 col-sm-offset-6 grid-form-option">
						<?= Html::submitButton($model->isNewRecord ? 'Save Configuration' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary','id' =>'submit']) ?>
                    </div>	
                </div>
                <div class="row">
                	<div class="col-sm-6">
						<?php echo \yii\helpers\Html::a( 'Back', Yii::$app->request->referrer, ['class' => 'btn btn-reset']);?>
                    </div>
                    <div class="col-sm-6 grid-form-option">
                        <button type="reset" class="btn btn-reset">RESET</button>
                    </div>
                </div>
            </div>
        </div>
        </div>
      </div>
    </div>
  </div>
  <div style="clear:both;"></div>
  <input type="hidden" id="page_is_dirty" name="page_is_dirty" value="0" />
</section>
<style>
  select option {color:#515151;}
</style>

<div class="gbox-nav hide">
  <ul>
    <li id="outlet" class="outlet" ><img src="<?php echo Url::home(true);?>images/outlet.svg">Outlet</li>
    <li id="catch-basin-selected" class="catch-basin-selected" ><img src="<?php echo Url::home(true);?>images/catch-basin-selected.svg">Catch Basin</li>
    <!--
    <li><img src="<?php echo Url::home(true);?>images/90-selected.svg">90 Selected</li>
    <li><img src="<?php echo Url::home(true);?>images/tee-selected.svg">Tee Selected</li>
    <li><img src="<?php echo Url::home(true);?>images/4-way-tee selected.svg">4-Way Tee Selected</li>
    -->
  </ul>
</div>
<div class="gline-box hide">
  <div class="grid_popover">
    <input type="number" name="length" placeholder="ft">
    <input type="button" value="OK">
  </div>
</div>
<?php ActiveForm::end(); ?>

