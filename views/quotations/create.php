<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Quotations */

$this->title = 'Create Quotations';
$this->params['breadcrumbs'][] = ['label' => 'Quotations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="quotations-create">


    <?= $this->render('_form', [
        'model' => $model,
        'modelFeatures'=>$modelFeatures,
        'modelFeature'=> $modelFeature,
        'modelGrateMaterial'=>$modelGrateMaterial,
        'modelGrateLoadRating'=>$modelGrateLoadRating,
        'modelCatchbasin'=>$modelCatchbasin,
        'modelCatchbasinsize'=>$modelCatchbasinsize,
    ]) ?>

</div>
