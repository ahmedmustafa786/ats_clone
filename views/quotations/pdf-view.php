<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\components\Utility;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\mpdf\Pdf;

$this->title = 'View Configuration';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
    <div class="pull-right"><h4><b>Label</b><br/><?php echo $quoteData->label;?></h4></div>
    <div class=" col-sm-4">
        
        <div class="row">
            <div class="col-lg-9"><h3><?php echo @implode('<br>', json_decode($quoteData->drain_model));?></h3></div>
            
        </div>
        <table class="table table-striped">
            <?php   $components = array();
                    $x = 0;
                    // \app\components\Utility::debug(json_decode($quoteData->details));exit;
                    foreach(json_decode($quoteData->details) as $bs){ $bs = (array)$bs;
                        // \app\components\Utility::debug($bs[1]);exit;
                        if(isset($bs[0]) && $bs[0] == ''){
                            if(is_array($bs[2])){
                                $bs[2] = (array)$bs[2];
                            }
                            foreach($bs[2] as $row){
                                foreach($row as $drain => $qty){
                                    $length = 0;
                                    if(strpos($drain, '_N12') > -1){
                                        $length = 1;
                                    }else if(strpos($drain, '_N24') > -1){
                                        $length = 2;
                                    }else if(strpos($drain, '_N36') > -1){
                                        $length = 3;
                                    }else if(strpos($drain, '_N48') > -1){
                                        $length = 4;
                                    }else{
                                        $length = 4;
                                    }
                                    $components[$bs[1]][$x][$drain] = $length.'___'.$qty;
                                }
                                $x++;
                            }
                        }
                        if(isset($bs[1]) && $bs[1] == ''){
                        ?>
                        <tr>
                            <td style="font-size:11px;"><?php echo $bs[2];?></td>
                            <td style="font-size:11px;"><?php echo $bs[0];?></td>                        
                        </tr>
                <?php   } ?>
            <?php } ?>
        </table>
        
    </div>
    <div class="col-sm-8">
        <h3>Design Layout</h3>
        {{canvas_photo_here}}
    </div>
    <div class="clear"></div>
    
</section>