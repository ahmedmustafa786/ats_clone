<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\QuotationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Configurations';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
  <div class="row">
    <h1 class="col-lg-4"><?= Html::encode("View Configurations") ?></h1>
    <div class="col-lg-8">
      <div class="pull-right">
        <?= Html::a('Add New', ['quotations/create','jobid'=>Yii::$app->request->get('id')], ['class' => 'btn btn-primary']) ?>
      </div>
    </div>
  </div>
</section>

<section class="content">
    <div class="quotations-index table-outer">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'label',
                [
                    'format' => 'raw',
                    'label' => 'Title',
                    'filter'=>'',
                    'value' =>function($data){
                        $title = json_decode($data->drain_model);
                        return $title[0];
                    }
                ],
                [
                    'format' => 'raw',
                    'label' => 'Status',
                    'filter'=>'',
                    'value' =>function($data){
                        if($data->status == '1'){
                            return $data->status0->title;
                        }else{ 
                            return "Not Requested";
                        } 
                    }
                ],
                'drain_model:ntext',
                'create_date',
    
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => "{view} {delete}",
                    'header'=> 'Action',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['quotations/view','id'=>$model->id,'jobid'=>Yii::$app->request->get('id')]);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['quotations/delete','id'=>$model->id],['data-confirm' => Yii::t('yii', 'Are you sure you want to delete this Member?'),'data-method'=>'post']);
                        }
                    ],
                ],
            ],'tableOptions' =>['class'=>'table table-striped table-paginate']
        ]); ?>
    </div>
</section>
