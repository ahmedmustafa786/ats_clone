<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Quotations */
$this->title = 'View Configuration';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
    <h1>
        <div class="col-sm-10">
            <?= Html::encode($this->title) ?>
        </div>
        <div class="col-sm-2">   
            <?php 
            if($quoteRow->status == 0){
                echo \yii\helpers\Html::a( 'Submit For Approval', ["quotations/submitapproval?recordId=".$quoteRow->id], ['class' => 'btn btn-info']);
            }else{
                if(in_array(3, $this->context->userroles)){
                ?>
                <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#action">Update Status</button>
                <?php
                }else{
                ?>
                <?php if($quoteRow->statuses->title == 'Awarded'){ ?>
                <div class="alert alert-success"><?php echo $quoteRow->statuses->title;?></div>
                <?php }else{ ?>
                <div class="alert alert-info" ><?php echo $quoteRow->statuses->title;?><span style="font-size: 15px; display:block;"><?php echo (!empty($quoteRow->reasons))?"Reason: ".$quoteRow->reasons:'';?></span></div>
                <?php } ?>
                <?php
                }
            }
            ?>
        </div>
    </h1>

    <div class=" col-sm-4">
        <div class="row">
            <div class="col-lg-9"><h3><?php echo @implode('<br>', json_decode($quoteRow->drain_model));?></h3></div>
            <div class="col-lg-3"><h4><b>Label</b><br/><?php echo $quoteRow->label;?></h4></div>
        </div>
        
        <table class="table table-striped jobs_table">
            <?php   $components = array();
                    $x = 0;
                    // \app\components\Utility::debug(json_decode($quoteRow->details));exit;
                    foreach(json_decode($quoteRow->details) as $bs){ $bs = (array)$bs;
                        // \app\components\Utility::debug($bs[1]);exit;
                        if(isset($bs[0]) && $bs[0] == ''){
                            if(is_array($bs[2])){
                                $bs[2] = (array)$bs[2];
                            }
                            foreach($bs[2] as $row){
                                foreach($row as $drain => $qty){
                                    $length = 0;
                                    if(strpos($drain, '_N12') > -1){
                                        $length = 1;
                                    }else if(strpos($drain, '_N24') > -1){
                                        $length = 2;
                                    }else if(strpos($drain, '_N36') > -1){
                                        $length = 3;
                                    }else if(strpos($drain, '_N48') > -1){
                                        $length = 4;
                                    }else{
                                        $length = 4;
                                    }
                                    $components[$bs[1]][$x][$drain] = $length.'___'.$qty;
                                }
                                $x++;
                            }
                        }
                        if(isset($bs[1]) && $bs[1] == ''){
                        ?>
                        <tr>
                            <td><?php echo $bs[2];?></td>
                            <td><?php echo $bs[0];?></td>                        
                        </tr>
                <?php   } ?>
            <?php } ?>
        </table>
        
    </div>
    <div class="col-sm-12">
        <h3>Design Layout</h3>
        <?php 
            if(isset($components['hor'])){
                $direction = 'hor';
            }else{
                $direction = 'var';
            }
            if(isset($components[$direction][0])){
                $componentIndex = 0;
            }else if(isset($components[$direction][1])){
                $componentIndex = 1;
            }
            $height = $width = '100';
            if(isset($components['var'])){
                $heightQty = 0;
                foreach($components['var'] as $lng){
                    foreach($lng as $key => $val){
                        $exploded = explode('___', $val);
                        $qty = $exploded[1];
                        $heightQty += $qty;
                    }
                }
                $height = $heightQty * 50;
            }
            if(isset($components['hor'])){
                $widthQty = 0;
                foreach($components['hor'] as $lng){
                    foreach($lng as $key => $val){
                        $exploded = explode('___', $val);
                        $qty = $exploded[1];
                        $widthQty += $qty;
                    }
                }
                $width  = ($widthQty+1) * 50;
            }
            
                ?>
        <div class="layout" style="min-height:<?php echo ($height < 500)?'500px':$height.'px';?>; width:<?php echo ($width < 500)?'500px':$width.'px';?>; overflow: scroll !important; position:relative; ">
            
        <?php $data = (array)json_decode($quoteRow->details);
              if(!empty($data['outletPosition'])){
              $outletPosition = $data['outletPosition'];
              }
              elseif(!empty($data['catchBasinPosition']))
              {
                $outletPosition = $data['catchBasinPosition'];
              }
              // \app\components\Utility::debug($data); exit;
              $y = 0;
              
            foreach($components as $dir => $result){
                // echo $dir;
                $totalComponents = count($result);
                $co = '';
                $coPositioning = '';
                foreach($outletPosition as $op){
                    $coPositioning .= "$op:0px;";
                }
                if(count($components) > 1
                && $y == 0){
                    if(!empty($data['outletPosition']))
                    {
                        $co = '<div class="layout-box" style="position:absolute; '.$coPositioning.'width:35px; height:40px; padding-top:8px; background:#efefef; float:left;">CO</div>';
                    }
                    elseif(!empty($data['catchBasinPosition']))
                    {
                        $co = '<div class="layout-box" style="position:absolute; '.$coPositioning.'width:35px; height:40px; padding-top:8px; background:#efefef; float:left;">CB</div>';
                    }
                }
                $x = 0;

                foreach($result as $d){
                    if($dir == 'var'){
                        $new = array();
                        foreach($d as $key => $val){
                            if(strpos($key, '_N1') > -1 || strpos($key, '_N2') > -1 || strpos($key, '_N3') > -1){

                                $new['n'][$key] = $val;
                            }else if(strpos($key, 'A') > -1){
                                $new['a'][$key] = $val;
                            }
                            else{
                                $new['o'][$key] = $val;
                            }
                        }
                        if(array_key_exists('a',$new)){
                            $dKeys = array_keys($new['a']);
                            if(strpos($dKeys[count($dKeys)-1], '_1A') > -1 || strpos($dKeys[count($dKeys)-1], '_AA') > -1){
                                $new['a'] = array_reverse($new['a']);
                            }
                            $d = $new['a'];
                        }
                        if(!empty($new['n'])){
                            $d = array_merge($d, $new['n']);
                        }
                        if(!empty($new['o'])){
                            $dKeys = array_keys($new['o']);
                            if(strpos($dKeys[count($dKeys)-1], '_1') > -1){
                                $new['o'] = array_reverse($new['o']);
                            }
                            $d = array_merge($d, $new['o']);
                        }
                    }
                    if($dir == 'hor'){
                        $new = array();
                        foreach($d as $key => $val){
                            if(strpos($key, '_N1') > -1 || strpos($key, '_N2') > -1 || strpos($key, '_N3') > -1){

                                $new['n'][$key] = $val;
                            }else if(strpos($key, 'A') > -1){
                                $new['a'][$key] = $val;
                            }
                            else{
                                $new['o'][$key] = $val;
                            }
                        }
                        if(array_key_exists('a',$new)){
                            $dKeys = array_keys($new['a']);
                            if(strpos($dKeys[count($dKeys)-1], '_1A') > -1 || strpos($dKeys[count($dKeys)-1], '_AA') > -1){
                                $new['a'] = array_reverse($new['a']);
                            }
                            $d = $new['a'];
                        }
                        
                        if(!empty($new['n'])){
                            $d = array_merge($d, $new['n']);
                        }
                        if(!empty($new['o'])){
                            $dKeys = array_keys($new['o']);
                            if(strpos($dKeys[count($dKeys)-1], '_1') > -1){
                                $new['o'] = array_reverse($new['o']);
                            }
                            $d = array_merge($d, $new['o']);
                        }
                        
                        
                    }
                    $x++;
                    // \app\components\Utility::debug($d);
                    if(//$y%2 == 1 &&
                     is_array($d)){
                        $d = (array)$d;
                        if(count($components) == 1 
                        && in_array('left', $outletPosition) //&& in_array('bottom',$outletPosition)
                        ){
                            $d = array_reverse($d);
                        }
                        if(count($components) == 1 
                        && in_array('top', $outletPosition)){ //&& in_array('right',$outletPosition)
                            $d = array_reverse($d);
                        }

                    }
                    $z = 0;
                    $outerstyle = '';
                    if(count($components) > 1){
                        $outerstyle .= 'position: absolute;';
                        if($dir == 'hor'){
                            if(in_array('left', $outletPosition)){
                                $outerstyle .= 'left:35px;';
                                if(in_array('top', $outletPosition)){
                                    $outerstyle .= 'top:0px;';
                                }else if(in_array('bottom', $outletPosition)){
                                    $outerstyle .= 'bottom:0px;';
                                }
                            }else if(in_array('right', $outletPosition)){
                                $outerstyle .= 'right:35px;';
                                if(in_array('top', $outletPosition)){
                                    $outerstyle .= 'top:0px;';
                                }else if(in_array('bottom', $outletPosition)){
                                    $outerstyle .= 'bottom:0px;';
                                }
                            }
                        }
                        if($dir == 'var'){
                            if(in_array('top', $outletPosition)){
                                $outerstyle .= 'top:40px;';
                                if(in_array('left', $outletPosition)){
                                    $outerstyle .= 'left:0px;';
                                }else if(in_array('right', $outletPosition)){
                                    $outerstyle .= 'right:0px;';
                                }
                            }else if(in_array('bottom', $outletPosition)){
                                $outerstyle .= 'bottom:40px;';
                                if(in_array('left', $outletPosition)){
                                    $outerstyle .= 'left:0px;';
                                }else if(in_array('right', $outletPosition)){
                                    $outerstyle .= 'right:0px;';
                                }
                            }
                        }
                    }

                    echo '<div class="length-outer" style="'.$outerstyle.' ">';
                    // $d = array_reverse($d);
                    if($totalComponents == 1 
                    && count($components) == 1
                    && in_array('left', $outletPosition)
                    ){
                        ?>
                    <?php if(!empty($data['catchBasinPosition'])){?>
                    <div class="layout-box <?php echo $dir;?>">CB</div>
                    <?php }else{?>

                    <div class="outlet <?php echo $dir;?>">Outlet</div><?php    
                    }}
                    if($totalComponents == 1 
                    && count($components) == 1
                    && in_array('top', $outletPosition)
                    ){
                        ?>
                    <?php if(!empty($data['catchBasinPosition'])){?>
                    <div class="layout-box <?php echo $dir;?>">CB</div>
                    <?php }else{?>
                    <div class="outlet <?php echo $dir;?>">Outlet</div><?php    
                    }}
                    if($totalComponents == 1 
                    && count($components) == 1
                    && empty($outletPosition)
                    ){
                        ?>
                    <?php if(!empty($data['catchBasinPosition'])){?>
                    <div class="layout-box <?php echo $dir;?>">CB</div>
                    <?php }else{?>
                    <div class="outlet <?php echo $dir;?>">Outlet</div><?php    
                    }}

                    if($totalComponents > 1
                    && $y == 0){
                        $d = array_reverse($d);
                    } 
                    foreach($d as $com => $len){
                        $quantity   = explode('___', $len);
                        $len        = $quantity[0];
                        $qty        = $quantity[1];
                        // echo count($qty);
                        // exit;
                        $size = 10; 
                        
                        for($x = 1; $x <= $qty; $x++){
                            //styling area
                            $var = $hor = '';
                            if($dir == 'var'){
                                $var = 'float:none;';
                            }else{
                                $hor = 'float:left;';
                            }

                            if($z == 0){
                                if($dir == 'var'
                                && count($components) > 1){
                                    // $var .= 'margin-top:50px;';
                                }

                                if($dir == 'hor'
                                && count($components) > 1){
                                    $hor .= '';
                                }
                            }
                        ?>
                        <div class="layout-box" style="<?php echo $var.$hor;?>height: 40px; padding-top:8px;">
                            <?php 
                                $explodedCom = explode('_', $com);
                                echo trim($explodedCom[count($explodedCom)-1]);
                                $sequence = '&nbsp;';
                                if(strpos($com, '_N') > -1){
                                    $sequence = $explodedCom[count($explodedCom)-2];
                                }
                            ?>
                            <div class="length <?php echo $dir;?>"><?php echo $len;?>'</div>
                            <div class="sequence"><?php echo $sequence;?></div>
                        </div>
                        <?php 
                            }
                            $z++; ?>
                    <?php } 
                    $y++;
                    echo '</div>';
                    // \app\components\Utility::debug($outletPosition);
                    if($totalComponents == 1 
                    && count($components) == 1
                    && in_array('right', $outletPosition)
                    && $dir == 'hor'){
                        ?>
                    <?php if(!empty($data['catchBasinPosition'])){?>
                    <div class="layout-box <?php echo $dir;?>">CB</div>
                    <?php }else{?>  
                    <div class="outlet <?php echo $dir;?>">Outlet</div>

                    <?php    
                    }}
                    if($totalComponents == 1 
                    && count($components) == 1
                    && in_array('bottom', $outletPosition)
                    && $dir == 'var'){ 
                        ?>
                    <?php if(!empty($data['catchBasinPosition'])){?>
                    <div class="layout-box <?php echo $dir;?>">CB</div>
                    <?php }else{?>
                    <div class="outlet <?php echo $dir;?>">Outlet</div><?php    
                    }}
                    
                    if($totalComponents > 1
                    && $y < $totalComponents
                    && count($components) == 1
                    ){
                    ?>
                    <?php if(!empty($data['catchBasinPosition'])){?>
                    <div class="layout-box <?php echo $dir;?>">CB</div>
                    <?php }else{?>
                    <div class="outlet <?php echo $dir;?>">Outlet</div><?php
                    }} 
                    echo $co;
                    $co = '';           
                }
                ?>                
            <?php } ?>
        </div>
    </div>
    <div class="clear"></div>
    <br><br>
    <?php  echo \yii\helpers\Html::a( 'Back', ["jobs/view?id=".$quoteRow->job_id], ['class' => 'btn btn-primary']);?>
    <?php if(!empty($quoteRow->filename)){ ?>
        <?php  echo \yii\helpers\Html::a( 'View Configuration & Bill of Material', ["quotes/".$quoteRow->filename], ['class' => 'btn btn-info', 'target' => '_blank']);?>
    <?php } else { ?>
    <?php  echo \yii\helpers\Html::a( 'Generate Configuration & Bill of Material', '#', ['data-toggle' => "modal", 'data-target' => "#loading", 'class' => 'btn btn-primary', 'id'=>'screenshot']);?>
    <?php } ?>
     <br><br>
</section>

<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<?php $this->registerJsFile(Url::home(true).'/js/html2canvas.js'); ?>
<?php $this->registerJsFile(Url::home(true).'/js/canvas2image.js'); ?>
<?php
    $csrfTokenName  = Yii::$app->request->csrfParam;
    $csrfToken      = Yii::$app->request->csrfToken;
?>
<script>
$(document).ready(function(){
    
    /*if($('.skin-blue').hasClass('sidebar-collapse')) {
      $('.skin-blue').removeClass('sidebar-collapse');
    } else {
      $('.skin-blue').addClass('sidebar-collapse');
    }
    $('.sidebar-menu a').mouseover(function(){
        $('.skin-blue').removeClass('sidebar-collapse');
    });
    $('.sidebar-menu a').mouseout(function(){
        $('.skin-blue').addClass('sidebar-collapse');
    });*/

    $("#screenshot").on('click', function(){
        
        html2canvas($(".layout"), {
            onrendered: function (canvas) {
                var canvasData = canvas.toDataURL("image/png");
                
                $.ajax({
                  type: "POST",
                  url: "generatepdf?recordId=<?php echo $quoteRow->id;?>",
                  data: {imgBase64: canvasData, <?php echo $csrfTokenName;?>:"<?php echo $csrfToken?>"}
                }).done(function(o) {
                  location.reload();
                });
            }
        });
    });
});
</script>
<style>
    .clear{clear:both;}
    .outlet{float:left; }
    .outlet.hor{height: 70px; padding-top:60px; width:2px;border-right: 1px dashed #515151;}
    .outlet.var{height: 2px; width:120px; text-align: right;border-bottom: 1px dashed #515151;}
    .layout-box.hor{height: 40px; padding-top:12px; width:2px;border-right: 1px dashed #515151; float: left; text-align: center;}
    .layout-box.var{height: 35px; width:120px; text-align: right;border-bottom: 1px dashed #515151;text-align: center;}
    .layout{border:1px solid #ccc; padding:5%; min-height: 300px; background: url('<?php echo Url::home(true);?>images/grid-box.png') repeat;}
    .layout .layout-box{width:35px; border:1px solid #666; text-align: center; }
    .layout .layout-box .length{text-align: center; position: relative;display:none;}
    .layout .layout-box .sequence{ text-align: center; position: relative;  font-size: 9px;}
    
    @media(max-width: 1280px){
        .col-sm-4 .btn{margin-bottom: 10px;}
    }
</style>
<div id="loading" class="model fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <img src="<?php echo Url::home(true);?>images/loading.gif">
          </div>
        </div>
      </div>
</div>
<div id="action" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Action Needed</h4>
      </div>
      <div class="modal-body">
        <?php 
            $form = ActiveForm::begin([
                'id' => 'update-form', 
                'action' => Url::home(true).'quotations/approve/?recordId='.$quoteRow->id,
            ]);
            ?>
        <div class="form-heading">
            <div class="row">                
                <div class="col-sm-6">
                    <label>Status</label>
                    <?= Html::activeDropDownList($quoteRow, 
                                        'status_id', 
                                        ArrayHelper::map(app\models\Statuses::find()->all(), 'id', 'title'),
                                        [
                                        'class' => 'form-control', 
                                        'prompt' => 'Select Status',
                                        'options' => [$quoteRow->status_id => ['selected' => true]]
                                        ]
                                        ) ?>
                </div>
                <div class="col-sm-6">
                    <?php echo $form->field($quoteRow, 'reasons')->textInput(['required' => 'required', 'value' => $quoteRow->reasons, 'class' => 'form-control']); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    
                </div>
                
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      <?php ActiveForm::end() ?>
    </div>
  </div>
</div>