<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Statuses */

$this->title = 'Create Statuses';
$this->params['breadcrumbs'][] = ['label' => 'Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
  <div class="row">
    <h1 class="col-lg-4">
      <?= Html::encode($this->title) ?>
    </h1>
    <div class="col-lg-8">
      <div class="pull-right"></div>
    </div>
  </div>
</section>
<section class="content">
  <div class="statuses-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
  </div>
</section>
