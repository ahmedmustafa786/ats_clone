<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Drainproducts */

$this->title = 'Create Drainproducts';
$this->params['breadcrumbs'][] = ['label' => 'Drainproducts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="drainproducts-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
