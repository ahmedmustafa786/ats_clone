<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Featuretypes */

$this->title = 'Create Featuretypes';
$this->params['breadcrumbs'][] = ['label' => 'Featuretypes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="featuretypes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
