<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Featuretypes */

$this->title = 'Update Featuretypes: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Featuretypes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="featuretypes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
