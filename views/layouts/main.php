<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\components\CommonHelper;
use yii\bootstrap\Alert;
AppAsset::register($this);
$this->beginPage();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="<?= Yii::$app->charset?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?= Html::csrfMetaTags() ?>
    <title>
      <?= Yii::$app->BaseComponent->settings['siteTitle'];?>
    </title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php $this->head() ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <?php $this->beginBody() ?>
    <?php $currentUrl = Yii::$app->request->getUrl();?>
    <?php if(!Yii::$app->user->isGuest){ ?>
    <div class="wrapper <?= ((Yii::$app->controller->id == 'quotations') && (Yii::$app->controller->action->id == 'create')) ? 'grid-wrapper' : '';?>">
      <header class="main-header"> 
        <?php if(((Yii::$app->controller->id == 'quotations') && (Yii::$app->controller->action->id == 'create'))){ ?>
        <?php }else{ ?>
        <a href="<?= Url::home(true);?>" class="logo" title="<?= Yii::$app->BaseComponent->settings['siteTitle'];?>"> 
          <span class="logo-mini">
            <img src="<?= Url::home(true);?>/images/logo-icon.png" alt="<?= Yii::$app->BaseComponent->settings['siteTitle'];?>">
          </span> 
          <span class="logo-lg">
            <img src="<?= Url::home(true);?>/images/logo.png" alt="<?= Yii::$app->BaseComponent->settings['siteTitle'];?>">
          </span> 
        </a>
        <?php } ?>
        <nav class="navbar navbar-static-top" role="navigation">
          <?php if(((Yii::$app->controller->id == 'quotations') && (Yii::$app->controller->action->id == 'create'))){ ?>
          <a href="<?= Url::home(true);?>" class="logo" title="<?= Yii::$app->BaseComponent->settings['siteTitle'];?>"> 
            <span class="logo-lg">
              <img src="<?= Url::home(true);?>/images/logo.png" alt="<?= Yii::$app->BaseComponent->settings['siteTitle'];?>">
            </span> 
          </a>
          <?php } ?>
          <?php if(((Yii::$app->controller->id == 'quotations') && (Yii::$app->controller->action->id == 'create'))){ ?>
          <a href="#" class="sidebar-toggle"> 
            <span class="sr-only">Toggle navigation
            </span> 
          </a> 
          <?php }else{ ?>
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> 
            <span class="sr-only">Toggle navigation
            </span> 
          </a>
          <?php } ?>
          <div class="navbar-custom-menu">
            <?php if(!Yii::$app->user->isGuest){ 
                $session          = Yii::$app->session;
                $selectedCountry  = $session->get('selectedCountry');
            ?>
            <ul class="nav navbar-nav">
              <li class="dropdown flag-dropdown"> 
                <a href="" title="<?= (isset($selectedCountry))?$selectedCountry['title']:'Select Country';?>" class="dropdown-toggle" data-toggle="dropdown"> 
                  <img src="<?= Url::home(true);?>/flags/<?= (isset($selectedCountry))?$selectedCountry['id'].'/'.$selectedCountry['flag']:'1/canada-flag-icon.svg';?>" alt="<?= $selectedCountry['title'];?>"> 
                </a>
                <ul class="dropdown-menu">
                  <?php foreach(Yii::$app->BaseComponent->countries as $country){ ?>
                  <li>
                    <a href="<?= Yii::$app->homeUrl;?>users/changecountry/?countryId=<?= $country->id;?>" title="Change Country">
                      <img src="<?= Url::home(true);?>/flags/<?= $country->id;?>/<?= $country->flag;?>" alt="<?= $country->title;?>"> 
                      <?= $country->title;?>
                    </a>
                  </li>
                  <?php } ?>
                  <li>
                    <a href="<?= Yii::$app->homeUrl;?>users/resetcountry" title="Reset Country">
                      <img src="<?= Url::home(true);?>/flags/reset-country-icon.png">Reset Country
                    </a>
                  </li>
                </ul>
              </li>
              <li class="dropdown notify-dropdown"> 
                <a href="" class="dropdown-toggle" data-toggle="dropdown" title="<?= (count(Yii::$app->BaseComponent->notifications) > 0)?count(Yii::$app->BaseComponent->notifications).' New Notifications':''?>"> 
                  <img src="<?= Url::home(true);?>/images/<?= (count(Yii::$app->BaseComponent->notifications) > 0)?'notifications-orange.png':'notification_icon.svg';?>" alt="">               
                </a> 
                <ul class="dropdown-menu">
                  <?php if(count(Yii::$app->BaseComponent->notifications) > 0){ ?>
                    <?php foreach(Yii::$app->BaseComponent->notifications as $key => $val){ ?>
                    <li>
                      <?= $val;?>
                    </li>
                    <?php } ?>
                  <?php } ?>
                  <li>
                    <a href="<?= Yii::$app->homeUrl;?>notifications/list" title="Show all Notifications">Show All
                    </a>
                  </li>
                </ul>
              </li>
              <li class="dropdown user-dropdown"> 
                <a href="" class="dropdown-toggle" data-toggle="dropdown"> 
                  <img src="<?= Url::home(true);?>/images/user_icon.svg" alt=""> My Account 
                  <img src="<?= Url::home(true);?>/images/menu-drop-arrow.svg" class="drop-arrow" alt=""> 
                </a> 
                <ul class="dropdown-menu">
                  <li>
                    <a href="<?= Yii::$app->homeUrl;?>users/update?recordId=<?= Yii::$app->user->getId();?>">Profile
                    </a>
                  </li>
                  <li>
                    <a href="<?= Yii::$app->homeUrl;?>users/password">Password
                    </a>
                  </li>
                  <li>
                    <a data-method="POST" href="<?= Yii::$app->homeUrl;?>site/logout">Logout
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
            <?php } ?>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <?php if(!Yii::$app->user->isGuest){ ?>
      <aside class="main-sidebar"> 
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar"> 
          <!-- Sidebar Menu -->
          <?php $currentUrl = Yii::$app->request->getUrl(); ?>
          <ul class="sidebar-menu">
            <!-- Optionally, you can add icons to the links -->
            <li class="<?= (strpos($currentUrl, '/jobs/index') > -1)?'active':'';?>">
              <a href="<?= Url::to(['/jobs/index']);?>"> 
                <i>
                  <img src="<?= Url::home(true);?>/images/jobs-icon.svg">
                </i> 
                <span>Existing Projects
                </span>
              </a>
            </li>
            <li class="<?= (strpos($currentUrl, '/jobs/create') > -1)?'active':'';?>">
              <a href="<?= Yii::$app->homeUrl;?>jobs/create"> 
                <i>
                  <img src="<?= Url::home(true);?>/images/jobs-icon.svg">
                </i> 
                <span>Add New Project
                </span>
              </a>
            </li>
            <!-- <li class="<?= (strpos($currentUrl, '/quotations/index') > -1)?'active':'';?>"><a href="<?= Url::to(['quotations/index']);?>"> <i><img src="<?= Url::home(true);?>/images/quotes-icon.svg"></i> <span>View Quoatations</span></a></li>-->
            <li class="treeview <?= (strpos($currentUrl, 'lengths/') > -1 || strpos($currentUrl, 'statuses/') > -1 || strpos($currentUrl, 'parts/') > -1)?'active':'';?>">
              <a href="#" >
                <i>
                  <img src="<?= Url::home(true);?>/images/dashboard-icon.svg">
                </i> 
                <span>Configuration
                </span> 
                <span class="pull-right-container"> 
                  <i class="fa fa-angle-left pull-right">
                  </i>
                </span> 
              </a>
              <ul  class="treeview-menu">
                <li class="<?= (strpos($currentUrl, '/lengths/index') > -1)?'active':'';?>">
                  <a title="Manage Lengths" href="<?= Url::to(['lengths/index']);?>">
                    <i class="fa fa-circle-o">
                    </i>Manage Lengths
                  </a>
                </li>
                <li class="<?= (strpos($currentUrl, 'statuses/index') > -1)?'active':'';?>">
                  <a title="Manage Statuses" href="<?= Url::to(['statuses/index']);?>">
                    <i class="fa fa-circle-o">
                    </i>Manage Statuses
                  </a>
                </li>
                <li class="<?= (strpos($currentUrl, 'parts/index') > -1)?'active':'';?>">
                  <a title="Manage Parts" href="<?= Url::to(['parts/index']);?>">
                    <i class="fa fa-circle-o">
                    </i>Manage Parts
                  </a>
                </li>
              </ul>
            </li>
            <li class="treeview <?= (strpos($currentUrl, 'cities/') > -1 || strpos($currentUrl, 'states/') > -1 || strpos($currentUrl, 'countries/') > -1 || strpos($currentUrl, 'timezones/') > -1)?'active':'';?>">
              <a href="#">
                <i>
                  <img src="<?= Url::home(true);?>/images/dashboard-icon.svg">
                </i> 
                <span>Geo Locations
                </span> 
                <span class="pull-right-container"> 
                  <i class="fa fa-angle-left pull-right">
                  </i>
                </span> 
              </a>
              <ul class="treeview-menu ">
                <li class="<?= (strpos($currentUrl, '/cities/index') > -1)?'active':'';?>">
                  <a title="Manage Cities" href="<?= Url::to(['/cities/index']);?>">
                    <i class="fa fa-circle-o">
                    </i> Manage Cities
                  </a>
                </li>
                <li class="<?= (strpos($currentUrl, '/states/index') > -1)?'active':'';?>">
                  <a title="Manage States" href="<?= Url::to(['/states/index']);?>">
                    <i class="fa fa-circle-o">
                    </i> Manage States
                  </a>
                </li>
                <li class="<?= (strpos($currentUrl, '/countries/index') > -1)?'active':'';?>">
                  <a title="Manage States" href="<?= Url::to(['/countries/index']);?>">
                    <i class="fa fa-circle-o">
                    </i> Manage Countries
                  </a>
                </li>
                <li class="<?= (strpos($currentUrl, '/timezones/index') > -1)?'active':'';?>">
                  <a title="Manage States" href="<?= Url::to(['/timezones/index']);?>">
                    <i class="fa fa-circle-o">
                    </i> Manage Timezones
                  </a>
                </li>
              </ul>
            </li>
            <li class="treeview <?= ((strpos($currentUrl, 'user/') > -1 || strpos($currentUrl, 'userroles/') > -1 || strpos($currentUrl, 'modules/') > -1 || strpos($currentUrl, 'modulelists/') > -1) && strpos($currentUrl, 'user/dashboard') === false)?'active':'';?>">
              <a href="#">
                <i>
                  <img src="<?= Url::home(true);?>/images/dashboard-icon.svg">
                </i> 
                <span>User Policies
                </span> 
                <span class="pull-right-container"> 
                  <i class="fa fa-angle-left pull-right">
                  </i>
                </span> 
              </a>
              <ul class="treeview-menu">
                <li class="<?= (strpos($currentUrl, '/user/index') > -1)?'active':'';?>">
                  <a title="Manage Users" href="<?= Url::to(['/user/index']);?>">
                    <i class="fa fa-circle-o">
                    </i> Users
                  </a>
                </li>
                <li class="<?= (strpos($currentUrl, '/admin/assignment') > -1)?'active':'';?>">
                  <a title="Manage Users" href="<?= Url::to(['/admin/assignment']);?>">
                    <i class="fa fa-circle-o">
                    </i> Users Assignment
                  </a>
                </li>
                <li class="<?= (strpos($currentUrl, '/admin/role') > -1)?'active':'';?>">
                  <a title="Manage Userroles" href="<?= Url::to(['/admin/role']);?>">
                    <i class="fa fa-circle-o">
                    </i> Manage Roles
                  </a>
                </li>
                <li class="<?= (strpos($currentUrl, '/admin/permission') > -1)?'active':'';?>">
                  <a title="Manage Modules" href="<?= Url::to(['/admin/permission']);?>">
                    <i class="fa fa-circle-o">
                    </i> Manage Permissions
                  </a>
                </li>
                <li class="<?= (strpos($currentUrl, '/admin/route') > -1)?'active':'';?>">
                  <a title="Manage Modulelists" href="<?= Url::to(['/admin/route']);?>">
                    <i class="fa fa-circle-o">
                    </i> Manage Routes
                  </a>
                </li>
              </ul>
            </li>
            <li class="treeview <?= (strpos($currentUrl, 'settings/') > -1 || strpos($currentUrl, 'companies/') > -1)?'active':'';?>">
              <a href="#">
                <i>
                  <img src="<?= Url::home(true);?>/images/dashboard-icon.svg">
                </i> 
                <span>Website Settings
                </span> 
                <span class="pull-right-container"> 
                  <i class="fa fa-angle-left pull-right">
                  </i>
                </span> 
              </a>
              <ul class="treeview-menu">
                <li class="<?= (strpos($currentUrl, 'settings/index') > -1)?'active':'';?>">
                  <a title="Manage Settings" href="<?= Url::to(['settings/index']);?>">
                    <i class="fa fa-circle-o">
                    </i> Site Settings
                  </a>
                </li>
                <li class="<?= (strpos($currentUrl, 'companies/index') > -1)?'active':'';?>">
                  <a title="Manage Companies" href="<?= Url::to(['companies/index']);?>">
                    <i class="fa fa-circle-o">
                    </i> Sales Agencies
                  </a>
                </li>
              </ul>
            </li>
            <li class="treeview <?= (strpos($currentUrl, 'companies/') > -1 )?'active':'';?>">
              <a href="#">
                <i>
                  <img src="<?= Url::home(true);?>/images/dashboard-icon.svg">
                </i> 
                <span>My Agency Settings
                </span> 
                <span class="pull-right-container"> 
                  <i class="fa fa-angle-left pull-right">
                  </i>
                </span> 
              </a>
              <ul class="treeview-menu">
                <li class="<?= (strpos($currentUrl, 'companies/update') > -1)?'active':'';?>">
                  <a title="Update Info" href="<?= Url::to(['companies/update?id='.Yii::$app->BaseComponent->companyId]);?>">
                    <i class="fa fa-circle-o">
                    </i> Agency Info
                  </a>
                </li>
                <li class="<?= (strpos($currentUrl, 'companies/index') > -1)?'active':'';?>">
                  <a title="Manage Sales Rep" href="<?= Url::to(['companies/index?id='.Yii::$app->BaseComponent->companyId]);?>">
                    <i class="fa fa-circle-o">
                    </i> Sales Agencies
                  </a>
                </li>
              </ul>
            </li>
          </ul>
          <!-- /.sidebar-menu --> 
        </section>
        <!-- /.sidebar --> 
      </aside>
      <?php } //added sidebar ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper"> 
        <?php if(Yii::$app->session->getFlash('success')){ ?>
        <div class="alert alert-success">
          <?= Yii::$app->session->getFlash('success'); ?>  
        </div>
        <?php }else if(Yii::$app->session->getFlash('error')){ ?>
        <div class="alert alert-danger">
          <?= Yii::$app->session->getFlash('error'); ?>  
        </div>
        <?php } ?>
        <?php //echo Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [], ]) ?>
        <?= $content ?>
        <!-- /.content --> 
      </div>
      <!-- /.content-wrapper --> 
    </div>
    <?php }else{ ?>
        <?= $content ?>
    <?php } ?>
    <?php $this->endBody() ?>
  </body>
  
  <?php if(!Yii::$app->user->isGuest){ ?>
  <?php $this->registerJsFile(Url::home(true).'/js/app.js'); ?>
  <?php } ?>
</html>
<?php $this->endPage() ?>
