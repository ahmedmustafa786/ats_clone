<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
    <div class="row">
        <h1 class="col-lg-4"><?= Html::encode($this->title) ?></h1>
        <div class="col-lg-8">
      <div class="pull-right">
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-primary']) ?>
      </div>
    </div>
    </div>
    
</section>
<section class="content">
  <div class="lengths-index table-outer">

  <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'f_name',
            'l_name',
            'phone',
            'email:email',
            // 'auth_key',
            // 'password_hash',
            // 'password_reset_token',
            // 'address:ntext',
            // 'city_id',
            // 'timezone_id:datetime',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
            // 'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


  </div>
</section>
