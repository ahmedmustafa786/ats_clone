<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FlowratedataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Flowratedatas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="flowratedata-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Flowratedata', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'product_no',
            'configuration',
            'length',
            'weight',
            'dim_a',
            'dim_b',
            'gpm',
            'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
