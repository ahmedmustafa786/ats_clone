<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Flowratedata */

$this->title = 'Create Flowratedata';
$this->params['breadcrumbs'][] = ['label' => 'Flowratedatas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="flowratedata-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
