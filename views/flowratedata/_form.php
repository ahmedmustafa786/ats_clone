<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Flowratedata */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="flowratedata-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'product_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'configuration')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'length')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'weight')->textInput() ?>

    <?= $form->field($model, 'dim_a')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dim_b')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gpm')->textInput() ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
