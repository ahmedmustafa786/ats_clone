<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FlowratedataSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="flowratedata-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'product_no') ?>

    <?= $form->field($model, 'configuration') ?>

    <?= $form->field($model, 'length') ?>

    <?= $form->field($model, 'weight') ?>

    <?php // echo $form->field($model, 'dim_a') ?>

    <?php // echo $form->field($model, 'dim_b') ?>

    <?php // echo $form->field($model, 'gpm') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'create_date') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'update_date') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
