<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Drains */

$this->title = 'Create Drains';
$this->params['breadcrumbs'][] = ['label' => 'Drains', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="drains-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
