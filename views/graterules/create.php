<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Graterules */

$this->title = 'Create Graterules';
$this->params['breadcrumbs'][] = ['label' => 'Graterules', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="graterules-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
