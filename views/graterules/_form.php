<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Graterules */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="graterules-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'series_id')->textInput() ?>

    <?= $form->field($model, 'rule_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'grate_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hardware_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'termvariable')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'termmultiplier')->textInput() ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
