<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Settings */

$this->title = 'Update Settings: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<section class="content-header">
  <div class="row">
    <h1 class="col-lg-4">
      <?= Html::encode($this->title) ?>
    </h1>
    <div class="col-lg-8">
      <div class="pull-right"></div>
    </div>
  </div>
</section>
<section class="content">
  <div class="settings-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
  </div>
</section>
