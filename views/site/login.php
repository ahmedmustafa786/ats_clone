<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="main">
  <div class="wrapper_login">
    <div class="container">
      <div class="row">
        <div class="site-login ats_login"> <a href="/" class="logo" title="Watts"><img src="/images/logo.png" alt="" class="img-responsive logo"></a>
          <?php $form = ActiveForm::begin([
			'id' => 'login-form',
			'layout' => 'horizontal',
			'fieldConfig' => [
				'template' => "<div class=\"col-xs-12\">{input}</div>\n<div class=\"col-xs-12\">{error}</div>",
			],
		]); ?>
          <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>
          <?= $form->field($model, 'password')->passwordInput() ?>
          <?= $form->field($model, 'rememberMe')->checkbox([
            	'template' => "<div class=\"col-xs-12\">{input} {label}</div>\n<div class=\"col-xs-12\">{error}</div>",
        	]) ?>
          <div class="form-group">
            <div class="col-xs-12">
              <?= Html::submitButton('Sign In', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
          </div>
          <p class="forgot"><a href="">Forgot Password</a></p>
          <?php ActiveForm::end(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
