<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CompaniesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Companies';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
  <div class="row">
    <h1 class="col-lg-4">
      <?= Html::encode($this->title) ?>
    </h1>
    <div class="col-lg-8">
      <div class="pull-right">
        <?= Html::a('Create Companies', ['create'], ['class' => 'btn btn-primary']) ?>
      </div>
    </div>
  </div>
</section>
<section class="content">
  <div class="companies-index table-outer">
    <?= GridView::widget([
		'options' =>['class' => 'table-responsive'],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            'user_id',
            'address:ntext',
            'city_id',
            'state_id',
            'country_id',
            'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
  </div>
</section>
