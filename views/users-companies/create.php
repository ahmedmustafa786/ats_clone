<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UsersCompanies */

$this->title = 'Create Users Companies';
$this->params['breadcrumbs'][] = ['label' => 'Users Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-companies-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
