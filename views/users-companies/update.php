<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UsersCompanies */

$this->title = 'Update Users Companies: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="users-companies-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
