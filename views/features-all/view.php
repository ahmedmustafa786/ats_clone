<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FeaturesAll */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Features Alls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="features-all-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'trench_frame',
            'grate_ada',
            'grate_heel_proof',
            'grate_material',
            'grate_load_rating',
            'suffix',
        ],
    ]) ?>

</div>
