<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FeaturesAllSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Features Alls';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="features-all-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Features All', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'trench_frame',
            'grate_ada',
            'grate_heel_proof',
            'grate_material',
            // 'grate_load_rating',
            // 'suffix',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
