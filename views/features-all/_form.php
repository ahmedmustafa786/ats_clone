<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FeaturesAll */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="features-all-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'trench_frame')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'grate_ada')->dropDownList([ 'yes' => 'Yes', 'no' => 'No', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'grate_heel_proof')->dropDownList([ 'yes' => 'Yes', 'no' => 'No', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'grate_material')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'grate_load_rating')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'suffix')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
