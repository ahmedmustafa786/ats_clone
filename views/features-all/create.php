<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FeaturesAll */

$this->title = 'Create Features All';
$this->params['breadcrumbs'][] = ['label' => 'Features Alls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="features-all-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
