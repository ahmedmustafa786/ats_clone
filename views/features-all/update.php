<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FeaturesAll */

$this->title = 'Update Features All: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Features Alls', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="features-all-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
