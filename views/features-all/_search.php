<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FeaturesAllSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="features-all-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'trench_frame') ?>

    <?= $form->field($model, 'grate_ada') ?>

    <?= $form->field($model, 'grate_heel_proof') ?>

    <?= $form->field($model, 'grate_material') ?>

    <?php // echo $form->field($model, 'grate_load_rating') ?>

    <?php // echo $form->field($model, 'suffix') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
