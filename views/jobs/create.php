<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Jobs */

$this->title = 'Add Project';
$this->params['breadcrumbs'][] = ['label' => 'Jobs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
  <div class="row">
    <h1 class="col-lg-4">
      <?= Html::encode($this->title) ?>
    </h1>
    <div class="col-lg-8">
      <div class="pull-right"></div>
    </div>
  </div>
</section>
<section class="content">
  <div class="row jobs-create">
    <?= $this->render('_form', [
        'model' => $model,
        'buildingClass'=>$buildingClass,
    ]) ?>
  </div>
</section>
