<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\JobsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Existing Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
  <div class="row">
    <h1 class="col-lg-4"><?= Html::encode($this->title) ?></h1>
    <div class="col-lg-8">
      <div class="pull-right">
        <?= Html::a('Add New Project', ['create'], ['class' => 'btn btn-primary']) ?>
      </div>
    </div>
  </div>
</section>

<section class="content">
	<div class="jobs-index">
    	<?= GridView::widget([
			'options' =>['class' => 'table-responsive'],
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],
				'title',
				[
					'attribute'=>'class_of_building',
					'format'=>'raw',
					'value'=>function($data){
						return ucfirst($data->class_of_building);
					},
				],
				'bid_date_deadline',            
				[
					'attribute'=>'address',
					'label' => 'Location',
					'format'=>'raw',
					'value'=>function($data){
						return $data->address." ".(($data->city) ? $data->city->title :false )." ".(($data->state) ? $data->state->title : false)." ".(($data->country)? $data->country->title :false );
					},
				],
				[
					'attribute'=>'created_by',
					'format'=>'raw',
					'value'=>function($data){
						return $data->createdBy->getFullName() ;
					},
				],
				'create_date',
				['class' => 'yii\grid\ActionColumn'],
	
			],'tableOptions' =>['class'=>'table table-striped']
		]); ?>
    </div>
</section>