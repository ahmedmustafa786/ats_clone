<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Jobs */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Jobs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
  <div class="row">
    <h1 class="col-lg-4">
      <?= Html::encode("Project Details") ?>
    </h1>
    <div class="col-lg-8">
      <div class="pull-right"><?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?></div>
    </div>
  </div>
</section>
<section class="content">
    <div class="jobs-view">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [                    
                    'label' => 'Project Title',
                    'format' => 'raw',
                    'value' => ucfirst($model->title)
                ],
                [                    
                    'label' => 'Building Class',
                    'format' => 'raw',
                    'value' => ucfirst($model->class_of_building)
                ],
                'bid_date_deadline',
                'engineer_name',
                'engineer_phone',
                'engineer_email:email',
                [                    
                    'label' => 'Country',
                    'format' => 'raw',
                    'value' => ucfirst(($model->country) ? $model->country->title : false)
                ],
                'address:ntext',
                [                    
                    'label' => 'City',
                    'format' => 'raw',
                    'value' => ucfirst(($model->city) ? $model->city->title : false)
                ],
                [                    
                    'label' => 'State',
                    'format' => 'raw',
                    'value' => ucfirst(($model->state) ? $model->state->title : false)
                ],
                [                    
                    'label' => 'Created By',
                    'format' => 'raw',
                    'value' => ucfirst($model->createdBy->getFullName())
                ],
            ],
        ]) ?>    
    </div>
</section>
<div style="margin-top:-3em">
	<?= Yii::$app->controller->renderPartial('/quotations/index',[
        'dataProvider' => $dataProviderQuotation,
        'searchModel' => $searchModelQuotation
    ]);?>
</div>

