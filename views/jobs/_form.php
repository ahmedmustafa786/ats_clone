<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Jobs */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="jobs-form table-outer">
  <?php $form = ActiveForm::begin(); ?>
  <div class="row">
    <div class="col-md-6">
      <fieldset>
        <legend>Project Information</legend>
        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'class_of_building')->dropDownList(ArrayHelper::map($buildingClass,'id','title'),['maxlength' => true]) ?>
        <?= $form->field($model, 'bid_date_deadline')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Bid End Date...'],
                'pluginOptions' => [
                    'todayHighlight' => true,
                    'autoclose' => true,
                    'format' => 'mm/dd/yyyy',
                ],
            ]);
            ?>
        <?= $form->field($model, 'company_id',['template'=>'{input}'])->hiddenInput(['value'=>Yii::$app->user->identity->usersCompanies?Yii::$app->user->identity->usersCompanies[0]->company_id:'']) ?>
        <?= $form->field($model, 'address',['template'=>'{input}'])->hiddenInput() ?>
        <?php
                echo $form->field($model, 'country_id')->dropDownList(ArrayHelper::map(app\models\Countries::find()->all(),'id','title'), ['id'=>'country-id','prompt' => 'Select Country',]);
 
                // States
                echo $form->field($model, 'state_id')->widget(DepDrop::classname(), [
                    'options'=>['id'=>'state-id'],
                    'pluginOptions'=>[
                        'depends'=>['country-id'],
                        'placeholder'=>'Select State',
                        'url'=>Url::to(['jobs/sub-states'])
                    ]
                ]);

                // Cities
                echo $form->field($model, 'city_id')->widget(DepDrop::classname(), [
                    'pluginOptions'=>[
                        'depends'=>['state-id'],
                        'placeholder'=>'Select city',
                        'url'=>Url::to(['jobs/sub-cities'])
                    ]
                ]);
             ?>
        <?= $form->field($model, 'status')->dropDownList(['1'=>'Active','0'=>'In Active'], ['id'=>'status-id','prompt' => 'Select Status',]); ?>
      </fieldset>
    </div>
    <div class="col-md-6">
      <fieldset>
        <legend>Engineer Information</legend>
        <?= $form->field($model, 'engineer_name')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'engineer_phone')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'engineer_email')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'file')->fileInput()?>
      </fieldset>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
      </div>
    </div>
  </div>
  <?php ActiveForm::end(); ?>
</div>
