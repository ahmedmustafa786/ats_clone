<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "features".
 *
 * @property integer $id
 * @property integer $featuretype_id
 * @property string $title
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $create_date
 * @property string $update_date
 * @property string $status
 *
 * @property Featuretypes $featuretype
 * @property User $createdBy
 * @property User $updatedBy
 */
class Features extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'features';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['featuretype_id', 'title', 'status'], 'required'],
            [['featuretype_id', 'created_by', 'updated_by'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 1],
            [['featuretype_id'], 'exist', 'skipOnError' => true, 'targetClass' => Featuretypes::className(), 'targetAttribute' => ['featuretype_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        
        if($this->isNewRecord){
            $this->created_by = Yii::$app->user->identity->id;
        }else{
            $this->created_by = $this->created_by;
            $this->updated_by = $this->updated_by;
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'featuretype_id' => 'Featuretype ID',
            'title' => 'Title',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeaturetype()
    {
        return $this->hasOne(Featuretypes::className(), ['id' => 'featuretype_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
