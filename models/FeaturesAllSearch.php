<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FeaturesAll;

/**
 * FeaturesAllSearch represents the model behind the search form about `app\models\FeaturesAll`.
 */
class FeaturesAllSearch extends FeaturesAll
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['trench_frame', 'grate_ada', 'grate_heel_proof', 'grate_material', 'grate_load_rating', 'suffix'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FeaturesAll::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'trench_frame', $this->trench_frame])
            ->andFilterWhere(['like', 'grate_ada', $this->grate_ada])
            ->andFilterWhere(['like', 'grate_heel_proof', $this->grate_heel_proof])
            ->andFilterWhere(['like', 'grate_material', $this->grate_material])
            ->andFilterWhere(['like', 'grate_load_rating', $this->grate_load_rating])
            ->andFilterWhere(['like', 'suffix', $this->suffix]);

        return $dataProvider;
    }
}
