<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Graterules;

/**
 * GraterulesSearch represents the model behind the search form about `app\models\Graterules`.
 */
class GraterulesSearch extends Graterules
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'series_id', 'created_by', 'updated_by'], 'integer'],
            [['rule_type', 'grate_type', 'hardware_code', 'termvariable', 'create_date', 'update_date', 'status'], 'safe'],
            [['termmultiplier'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Graterules::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'series_id' => $this->series_id,
            'termmultiplier' => $this->termmultiplier,
            'create_date' => $this->create_date,
            'created_by' => $this->created_by,
            'update_date' => $this->update_date,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'rule_type', $this->rule_type])
            ->andFilterWhere(['like', 'grate_type', $this->grate_type])
            ->andFilterWhere(['like', 'hardware_code', $this->hardware_code])
            ->andFilterWhere(['like', 'termvariable', $this->termvariable])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
