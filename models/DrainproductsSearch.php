<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Drainproducts;

/**
 * DrainproductsSearch represents the model behind the search form about `app\models\Drainproducts`.
 */
class DrainproductsSearch extends Drainproducts
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'drain_id', 'created_by', 'updated_by'], 'integer'],
            [['type', 'product_code', 'create_date', 'update_date', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Drainproducts::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'drain_id' => $this->drain_id,
            'create_date' => $this->create_date,
            'created_by' => $this->created_by,
            'update_date' => $this->update_date,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'product_code', $this->product_code])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
