<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "features_all".
 *
 * @property integer $id
 * @property string $trench_frame
 * @property string $grate_ada
 * @property string $grate_heel_proof
 * @property string $grate_material
 * @property string $grate_load_rating
 * @property string $suffix
 */
class FeaturesAll extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'features_all';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_frametype','trench_frame', 'grate_ada', 'grate_heel_proof', 'grate_material', 'grate_load_rating', 'suffix'], 'required'],
            [['grate_ada', 'grate_heel_proof'], 'string'],
            [['trench_frame', 'grate_load_rating', 'suffix'], 'string', 'max' => 100],
            [['grate_material'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fk_featuretype' => 'Trench Frame & Width',
            'grate_ada' => 'Grate ADA Compliant',
            'grate_heel_proof' => 'Grate Heel Proof',
            'grate_material' => 'Grate Material',
            'grate_load_rating' => 'Grate Load Rating',
            'suffix' => 'Suffix',
        ];
    }
}
