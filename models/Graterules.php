<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "graterules".
 *
 * @property integer $id
 * @property integer $series_id
 * @property string $rule_type
 * @property string $grate_type
 * @property string $hardware_code
 * @property string $termvariable
 * @property double $termmultiplier
 * @property string $create_date
 * @property integer $created_by
 * @property string $update_date
 * @property integer $updated_by
 * @property string $status
 *
 * @property Series $series
 * @property User $createdBy
 * @property User $updatedBy
 */
class Graterules extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'graterules';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['series_id', 'rule_type', 'grate_type', 'hardware_code', 'termvariable', 'termmultiplier', 'status'], 'required'],
            [['series_id', 'created_by', 'updated_by'], 'integer'],
            [['termmultiplier'], 'number'],
            [['create_date', 'update_date'], 'safe'],
            [['rule_type', 'grate_type', 'termvariable'], 'string', 'max' => 20],
            [['hardware_code'], 'string', 'max' => 10],
            [['status'], 'string', 'max' => 1],
            [['series_id'], 'exist', 'skipOnError' => true, 'targetClass' => Series::className(), 'targetAttribute' => ['series_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        
        if($this->isNewRecord){
            $this->created_by = Yii::$app->user->identity->id;
        }else{
            $this->created_by = $this->created_by;
            $this->updated_by = $this->updated_by;
        }
        return true;
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'series_id' => 'Series ID',
            'rule_type' => 'Rule Type',
            'grate_type' => 'Grate Type',
            'hardware_code' => 'Hardware Code',
            'termvariable' => 'Termvariable',
            'termmultiplier' => 'Termmultiplier',
            'create_date' => 'Create Date',
            'created_by' => 'Created By',
            'update_date' => 'Update Date',
            'updated_by' => 'Updated By',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeries()
    {
        return $this->hasOne(Series::className(), ['id' => 'series_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
