<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "series".
 *
 * @property integer $id
 * @property string $title
 * @property integer $created_by
 * @property string $create_date
 * @property integer $updated_by
 * @property string $update_date
 * @property string $status
 *
 * @property Drains[] $drains
 * @property Graterules[] $graterules
 * @property User $createdBy
 * @property User $updatedBy
 */
class Series extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'series';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'status'], 'required'],
            [['created_by', 'updated_by'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 1],
            [['title'], 'unique'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        
        if($this->isNewRecord){
            $this->created_by = Yii::$app->user->identity->id;
        }else{
            $this->created_by = $this->created_by;
            $this->updated_by = $this->updated_by;
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'created_by' => 'Created By',
            'create_date' => 'Create Date',
            'updated_by' => 'Updated By',
            'update_date' => 'Update Date',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrains()
    {
        return $this->hasMany(Drains::className(), ['series_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGraterules()
    {
        return $this->hasMany(Graterules::className(), ['series_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
    //Get Series id for Catch Basin by AM (Start)
    public function getCatchBasin()
    {
        return $this->hasMany(Catchbasin::className(), ['series_id' => 'id']);
    }
    //Get Series id for Catch Basin by AM (End)

}
