<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notifications".
 *
 * @property integer $id
 * @property integer $userrole_id
 * @property integer $user_id
 * @property string $notificationtext
 * @property string $is_read
 * @property string $update_date
 * @property string $create_date
 * @property integer $updated_by
 * @property integer $created_by
 * @property string $status
 *
 * @property Userroles $userrole
 * @property User $user
 * @property User $updatedBy
 * @property User $createdBy
 */
class Notifications extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notifications';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userrole_id', 'user_id'], 'integer'],
            [['notificationtext', 'is_read', 'update_date', 'create_date', 'status'], 'required'],
            [['notificationtext'], 'string'],
            [['update_date', 'create_date'], 'safe'],
            [['is_read', 'status'], 'string', 'max' => 1],
            [['userrole_id'], 'exist', 'skipOnError' => true, 'targetClass' => Userroles::className(), 'targetAttribute' => ['userrole_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        
        if($this->isNewRecord){
            $this->created_by = Yii::$app->user->identity->id;
        }else{
            $this->created_by = $this->created_by;
            $this->updated_by = $this->updated_by;
        }
        return true;
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userrole_id' => 'Userrole ID',
            'user_id' => 'User ID',
            'notificationtext' => 'Notificationtext',
            'is_read' => 'Is Read',
            'update_date' => 'Update Date',
            'create_date' => 'Create Date',
            'updated_by' => 'Updated By',
            'created_by' => 'Created By',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserrole()
    {
        return $this->hasOne(Userroles::className(), ['id' => 'userrole_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
}
