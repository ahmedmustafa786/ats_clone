<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "featuretypes".
 *
 * @property integer $id
 * @property string $title
 * @property string $identifier
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $create_date
 * @property string $update_date
 * @property string $status
 *
 * @property Features[] $features
 * @property User $createdBy
 * @property User $updatedBy
 */
class Featuretypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'featuretypes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
        ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeatures()
    {
        return $this->hasMany(Features::className(), ['featuretype_id' => 'id']);
    }
}
