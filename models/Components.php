<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "components".
 *
 * @property integer $id
 * @property integer $drain_id
 * @property string $product_code
 * @property integer $quantity
 * @property integer $created_by
 * @property string $create_date
 * @property string $update_date
 * @property integer $updated_by
 * @property string $status
 *
 * @property Drains $drain
 * @property User $createdBy
 * @property User $updatedBy
 */
class Components extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'components';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['drain_id', 'product_code', 'quantity', 'status'], 'required'],
            [['drain_id', 'quantity', 'created_by', 'updated_by'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['product_code'], 'string', 'max' => 10],
            [['status'], 'string', 'max' => 1],
            [['drain_id'], 'exist', 'skipOnError' => true, 'targetClass' => Drains::className(), 'targetAttribute' => ['drain_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        
        if($this->isNewRecord){
            $this->created_by = Yii::$app->user->identity->id;
        }else{
            $this->created_by = $this->created_by;
            $this->updated_by = $this->updated_by;
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'drain_id' => 'Drain ID',
            'product_code' => 'Product Code',
            'quantity' => 'Quantity',
            'created_by' => 'Created By',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
            'updated_by' => 'Updated By',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrain()
    {
        return $this->hasOne(Drains::className(), ['id' => 'drain_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
