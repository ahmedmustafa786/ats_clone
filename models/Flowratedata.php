<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "flowratedata".
 *
 * @property integer $id
 * @property string $product_no
 * @property string $configuration
 * @property string $length
 * @property integer $weight
 * @property string $dim_a
 * @property string $dim_b
 * @property integer $gpm
 * @property integer $created_by
 * @property string $create_date
 * @property integer $updated_by
 * @property string $update_date
 * @property string $status
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class Flowratedata extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'flowratedata';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_no', 'configuration', 'length', 'weight', 'dim_a', 'dim_b', 'gpm', 'status'], 'required'],
            [['weight','series_id', 'gpm', 'created_by', 'updated_by'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['product_no', 'configuration', 'length', 'dim_a', 'dim_b'], 'string', 'max' => 20],
            [['status'], 'string', 'max' => 1],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        
        if($this->isNewRecord){
            $this->created_by = Yii::$app->user->identity->id;
        }else{
            $this->created_by = $this->created_by;
            $this->updated_by = $this->updated_by;
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_no' => 'Product No',
            'configuration' => 'Configuration',
            'length' => 'Length',
            'weight' => 'Weight',
            'dim_a' => 'Dim A',
            'dim_b' => 'Dim B',
            'gpm' => 'Gpm',
            'created_by' => 'Created By',
            'create_date' => 'Create Date',
            'updated_by' => 'Updated By',
            'update_date' => 'Update Date',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
