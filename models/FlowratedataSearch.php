<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Flowratedata;

/**
 * FlowratedataSearch represents the model behind the search form about `app\models\Flowratedata`.
 */
class FlowratedataSearch extends Flowratedata
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'weight', 'gpm', 'created_by', 'updated_by'], 'integer'],
            [['product_no', 'configuration', 'length', 'dim_a', 'dim_b', 'create_date', 'update_date', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Flowratedata::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'series_id'=>$this->series_id,
            'weight' => $this->weight,
            'gpm' => $this->gpm,
            'created_by' => $this->created_by,
            'create_date' => $this->create_date,
            'updated_by' => $this->updated_by,
            'update_date' => $this->update_date,
        ]);

        $query->andFilterWhere(['like', 'product_no', $this->product_no])
            ->andFilterWhere(['like', 'configuration', $this->configuration])
            ->andFilterWhere(['like', 'length', $this->length])
            ->andFilterWhere(['like', 'dim_a', $this->dim_a])
            ->andFilterWhere(['like', 'dim_b', $this->dim_b])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
