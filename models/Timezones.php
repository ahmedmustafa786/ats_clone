<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "timezones".
 *
 * @property integer $id
 * @property integer $country_id
 * @property string $title
 * @property integer $created_by
 * @property string $create_date
 * @property integer $updated_by
 * @property string $update_date
 * @property string $status
 *
 * @property Countries $country
 * @property User $createdBy
 * @property User $updatedBy
 * @property User[] $User
 */
class Timezones extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'timezones';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'title', 'status'], 'required'],
            [['country_id', 'created_by', 'updated_by'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['title'], 'string', 'max' => 20],
            [['status'], 'string', 'max' => 1],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Countries::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        
        if($this->isNewRecord){
            $this->created_by = Yii::$app->user->identity->id;
        }else{
            $this->created_by = $this->created_by;
            $this->updated_by = $this->updated_by;
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country_id' => 'Country ID',
            'title' => 'Title',
            'created_by' => 'Created By',
            'create_date' => 'Create Date',
            'updated_by' => 'Updated By',
            'update_date' => 'Update Date',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Countries::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasMany(User::className(), ['timezone_id' => 'id']);
    }
}
