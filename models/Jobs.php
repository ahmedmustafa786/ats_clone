<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jobs".
 *
 * @property integer $id
 * @property string $title
 * @property string $class_of_building
 * @property string $bid_date_deadline
 * @property string $engineer_name
 * @property string $engineer_phone
 * @property string $engineer_email
 * @property integer $country_id
 * @property string $address
 * @property integer $city_id
 * @property integer $state_id
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $create_date
 * @property string $update_date
 * @property string $status
 *
 * @property Countries $country
 * @property Cities $city
 * @property States $state
 * @property User $createdBy
 * @property User $updatedBy
 * @property Quotations[] $quotations
 */
class Jobs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jobs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'class_of_building', 'bid_date_deadline', 'engineer_name', 'engineer_phone', 'engineer_email', 'status'], 'required'],
            [['bid_date_deadline', 'create_date', 'update_date'], 'safe'],
            [['country_id', 'city_id', 'state_id', 'created_by', 'updated_by','company_id'], 'integer'],
            [['address','file'], 'string'],
            ['engineer_email','email'],
            [['file'], 'file', 'extensions' => 'jpg,png,doc,docx,pdf,txt','mimeTypes' => 'image/jpeg, image/png, application/msword, application/pdf, application/vnd.openxmlformats-officedocument.wordprocessingml.document, text/plain', 'skipOnEmpty' => true],
            [['title', 'engineer_name', 'engineer_email'], 'string', 'max' => 255],
            [['class_of_building', 'status'], 'string', 'max' => 1],
            [['engineer_phone'], 'string', 'max' => 50],
            [['title'], 'unique'],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Countries::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => States::className(), 'targetAttribute' => ['state_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Project Title',
            'class_of_building' => 'Building Class',
            'bid_date_deadline' => 'Bid Date Deadline',
            'file'=>'Additional File',
            'engineer_name' => 'Contractor Name',
            'engineer_phone' => 'Contractor Phone',
            'engineer_email' => 'Contractor Email',
            'company_id'=>'Company',
            'country_id' => 'Country',
            'address' => 'Address',
            'city_id' => 'City',
            'state_id' => 'State / Province',
            'status' => 'Status',
        ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        
        if($this->isNewRecord){
            $this->created_by = Yii::$app->user->identity->id;
        }else{
            $this->created_by = $this->created_by;
            $this->updated_by = $this->updated_by;
        }
        /*echo "<pre>";
        print_r($this);
        exit;*/
        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Countries::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(States::className(), ['id' => 'state_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuotations()
    {
        return $this->hasMany(Quotations::className(), ['job_id' => 'id']);
    }
}
