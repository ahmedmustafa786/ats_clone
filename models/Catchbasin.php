<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "catchbasin".
 *
 * @property int $id
 * @property int $series_id
 * @property string $title
 * @property string $trash_basket
 * @property int $catch_basin_size
 * @property int $length
 * @property int $created_by
 * @property string $create_date
 * @property int $updated_by
 * @property string $update_date
 * @property int $status
 *
 * @property Series $series
 */
class Catchbasin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'catchbasin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['series_id', 'title', 'length', 'created_by', 'create_date', 'updated_by', 'update_date', 'status'], 'required'],
            [['series_id', 'length', 'created_by', 'updated_by', 'status'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['trash_basket'], 'string', 'max' => 55],
            [['series_id'], 'exist', 'skipOnError' => true, 'targetClass' => Series::className(), 'targetAttribute' => ['series_id' => 'id']],
            [['catch_basin_size'], 'string','max'=>55],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'series_id' => 'Series ID',
            'title' => 'Title',
            'trash_basket' => 'Trash Basket',
            'catch_basin_size' => 'Catch Basin Size',
            'length' => 'Length',
            'created_by' => 'Created By',
            'create_date' => 'Create Date',
            'updated_by' => 'Updated By',
            'update_date' => 'Update Date',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeries()
    {
        return $this->hasOne(Series::className(), ['id' => 'series_id']);
    }
}
