<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "quotations".
 *
 * @property integer $id
 * @property integer $job_id
 * @property string $filename
 * @property string $details
 * @property integer $status_id
 * @property string $drain_model
 * @property string $create_date
 * @property integer $created_by
 * @property string $update_date
 * @property integer $updated_by
 * @property string $status
 *
 * @property Statuses $status0
 * @property User $createdBy
 * @property User $updatedBy
 * @property Jobs $job
 */
class Quotations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'quotations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['job_id', 'label', 'details', 'status_id', 'drain_model', 'status'], 'required'],
            [['job_id', 'status_id', 'flow_rate', 'created_by', 'updated_by'], 'integer'],
            [['filename', 'details', 'drain_model','reasons', 'frame_guard'], 'string'],
            [['create_date', 'update_date'], 'safe'],
            [['status'], 'string', 'max' => 1],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Statuses::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['job_id'], 'exist', 'skipOnError' => true, 'targetClass' => Jobs::className(), 'targetAttribute' => ['job_id' => 'id']],
        ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        
        if($this->isNewRecord){
            $this->created_by = Yii::$app->user->identity->id;
        }else{
            $this->created_by = $this->created_by;
            $this->updated_by = $this->updated_by;
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'job_id' => 'Job ID',
            'filename' => 'Filename',
            'details' => 'Details',
            'status_id' => 'Status ID',
            'drain_model' => 'Drain Model',
            'frame_guard' => 'Frame Guard',
            'flow_rate' => 'Flow Rate',
            'label' => 'Label',
            'create_date' => 'Create Date',
            'created_by' => 'Created By',
            'update_date' => 'Update Date',
            'updated_by' => 'Updated By',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus0()
    {
        return $this->hasOne(Statuses::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJob()
    {
        return $this->hasOne(Jobs::className(), ['id' => 'job_id']);
    }
}
