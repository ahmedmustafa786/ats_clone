<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "parts".
 *
 * @property integer $id
 * @property string $prod_no
 * @property string $code
 * @property string $description
 * @property double $price
 * @property integer $created_by
 * @property string $create_date
 * @property integer $updated_by
 * @property string $update_date
 * @property string $status
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class Parts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prod_no', 'code', 'description', 'price', 'status'], 'required'],
            [['description'], 'string'],
            [['price'], 'number'],
            [['created_by', 'updated_by'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['prod_no'], 'string', 'max' => 255],
            [['code'], 'string', 'max' => 20],
            [['status'], 'string', 'max' => 1],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        
        if($this->isNewRecord){
            $this->created_by = Yii::$app->user->identity->id;
        }else{
            $this->created_by = $this->created_by;
            $this->updated_by = $this->updated_by;
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'prod_no' => 'Prod No',
            'code' => 'Code',
            'description' => 'Description',
            'price' => 'Price',
            'created_by' => 'Created By',
            'create_date' => 'Create Date',
            'updated_by' => 'Updated By',
            'update_date' => 'Update Date',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
