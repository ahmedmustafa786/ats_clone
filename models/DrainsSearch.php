<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Drains;

/**
 * DrainsSearch represents the model behind the search form about `app\models\Drains`.
 */
class DrainsSearch extends Drains
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'series_id', 'length', 'created_by', 'updated_by'], 'integer'],
            [['title', 'config', 'end', 'create_date', 'update_date', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Drains::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'series_id' => $this->series_id,
            'length' => $this->length,
            'created_by' => $this->created_by,
            'create_date' => $this->create_date,
            'updated_by' => $this->updated_by,
            'update_date' => $this->update_date,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'config', $this->config])
            ->andFilterWhere(['like', 'end', $this->end])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
